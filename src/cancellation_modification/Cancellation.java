package cancellation_modification;

import java.util.ArrayList;

public class Cancellation {
	
	public boolean isCancelReportLoaded = true; 
	public String reservationNo_1;
	public String reservationNo_2;
	public String cancellationStatus;
	public String cancellationComment;
	public String customerName;
	public String rseervationNo_3;
	public String paymentType;
	public String programName_1;
	public String activityName_1;
	public String reservationDate;
	public String activity_Session;
	public ArrayList<String> cancellationPolicy;
	public String programName_2;
	public String programName_3;
	public String activityName_2;
	public String activity_ratePlan;
	public String activity_QTY;
	public String activity_RateUSD;
	public String activity_TotalValue;
	public String activity_RateCurrency;
	public String activity_TotalValueCurrency;
	public String activity_currency_1;
	public String activity_currency_2;
	public String CancellationCharge_program;
	public String CancellationCharge_customer;
	public String programCancellationNo;
	public String customer_Mail;
	public String total_ProgramCancellationCharge;
	public String total_CustomerCancellationCharge;
	public String additional_cancellationCharge;
	public String GSTCharge;
	public String TotalCharge;
	public String cancellationReason;
	public String internalNotes;
	
	
	public boolean isCancelReportLoaded() {
		return isCancelReportLoaded;
	}
	public void setCancelReportLoaded(boolean isCancelReportLoaded) {
		this.isCancelReportLoaded = isCancelReportLoaded;
	}
	public String getActivity_RateCurrency() {
		return activity_RateCurrency;
	}
	public void setActivity_RateCurrency(String activity_RateCurrency) {
		this.activity_RateCurrency = activity_RateCurrency;
	}
	public String getActivity_TotalValueCurrency() {
		return activity_TotalValueCurrency;
	}
	public void setActivity_TotalValueCurrency(String activity_TotalValueCurrency) {
		this.activity_TotalValueCurrency = activity_TotalValueCurrency;
	}
	public String getReservationNo_1() {
		return reservationNo_1;
	}
	public void setReservationNo_1(String reservationNo_1) {
		this.reservationNo_1 = reservationNo_1;
	}
	public String getReservationNo_2() {
		return reservationNo_2;
	}
	public void setReservationNo_2(String reservationNo_2) {
		this.reservationNo_2 = reservationNo_2;
	}
	public String getCancellationStatus() {
		return cancellationStatus;
	}
	public void setCancellationStatus(String cancellationStatus) {
		this.cancellationStatus = cancellationStatus;
	}
	public String getCancellationComment() {
		return cancellationComment;
	}
	public void setCancellationComment(String cancellationComment) {
		this.cancellationComment = cancellationComment;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getRseervationNo_3() {
		return rseervationNo_3;
	}
	public void setRseervationNo_3(String rseervationNo_3) {
		this.rseervationNo_3 = rseervationNo_3;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getProgramName_1() {
		return programName_1;
	}
	public void setProgramName_1(String programName_1) {
		this.programName_1 = programName_1;
	}
	public String getActivityName_1() {
		return activityName_1;
	}
	public void setActivityName_1(String activityName_1) {
		this.activityName_1 = activityName_1;
	}
	public String getReservationDate() {
		return reservationDate;
	}
	public void setReservationDate(String reservationDate) {
		this.reservationDate = reservationDate;
	}
	public String getActivity_Session() {
		return activity_Session;
	}
	public void setActivity_Session(String activity_Session) {
		this.activity_Session = activity_Session;
	}
	public ArrayList<String> getCancellationPolicy() {
		return cancellationPolicy;
	}
	public void setCancellationPolicy(ArrayList<String> cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}
	public String getProgramName_2() {
		return programName_2;
	}
	public void setProgramName_2(String programName_2) {
		this.programName_2 = programName_2;
	}
	public String getProgramName_3() {
		return programName_3;
	}
	public void setProgramName_3(String programName_3) {
		this.programName_3 = programName_3;
	}
	public String getActivityName_2() {
		return activityName_2;
	}
	public void setActivityName_2(String activityName_2) {
		this.activityName_2 = activityName_2;
	}
	public String getActivity_ratePlan() {
		return activity_ratePlan;
	}
	public void setActivity_ratePlan(String activity_ratePlan) {
		this.activity_ratePlan = activity_ratePlan;
	}
	public String getActivity_QTY() {
		return activity_QTY;
	}
	public void setActivity_QTY(String activity_QTY) {
		this.activity_QTY = activity_QTY;
	}
	public String getActivity_RateUSD() {
		return activity_RateUSD;
	}
	public void setActivity_RateUSD(String activity_RateUSD) {
		this.activity_RateUSD = activity_RateUSD;
	}
	public String getActivity_TotalValue() {
		return activity_TotalValue;
	}
	public void setActivity_TotalValue(String activity_TotalValue) {
		this.activity_TotalValue = activity_TotalValue;
	}
	public String getActivity_currency_1() {
		return activity_currency_1;
	}
	public void setActivity_currency_1(String activity_currency_1) {
		this.activity_currency_1 = activity_currency_1;
	}
	public String getActivity_currency_2() {
		return activity_currency_2;
	}
	public void setActivity_currency_2(String activity_currency_2) {
		this.activity_currency_2 = activity_currency_2;
	}
	public String getCancellationCharge_program() {
		return CancellationCharge_program;
	}
	public void setCancellationCharge_program(String cancellationCharge_program) {
		CancellationCharge_program = cancellationCharge_program;
	}
	public String getCancellationCharge_customer() {
		return CancellationCharge_customer;
	}
	public void setCancellationCharge_customer(String cancellationCharge_customer) {
		CancellationCharge_customer = cancellationCharge_customer;
	}
	public String getProgramCancellationNo() {
		return programCancellationNo;
	}
	public void setProgramCancellationNo(String programCancellationNo) {
		this.programCancellationNo = programCancellationNo;
	}
	public String getCustomer_Mail() {
		return customer_Mail;
	}
	public void setCustomer_Mail(String customer_Mail) {
		this.customer_Mail = customer_Mail;
	}
	public String getTotal_ProgramCancellationCharge() {
		return total_ProgramCancellationCharge;
	}
	public void setTotal_ProgramCancellationCharge(
			String total_ProgramCancellationCharge) {
		this.total_ProgramCancellationCharge = total_ProgramCancellationCharge;
	}
	public String getTotal_CustomerCancellationCharge() {
		return total_CustomerCancellationCharge;
	}
	public void setTotal_CustomerCancellationCharge(
			String total_CustomerCancellationCharge) {
		this.total_CustomerCancellationCharge = total_CustomerCancellationCharge;
	}
	public String getAdditional_cancellationCharge() {
		return additional_cancellationCharge;
	}
	public void setAdditional_cancellationCharge(
			String additional_cancellationCharge) {
		this.additional_cancellationCharge = additional_cancellationCharge;
	}
	public String getGSTCharge() {
		return GSTCharge;
	}
	public void setGSTCharge(String gSTCharge) {
		GSTCharge = gSTCharge;
	}
	public String getTotalCharge() {
		return TotalCharge;
	}
	public void setTotalCharge(String totalCharge) {
		TotalCharge = totalCharge;
	}
	public String getCancellationReason() {
		return cancellationReason;
	}
	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	
	
	
	
	
	

}
