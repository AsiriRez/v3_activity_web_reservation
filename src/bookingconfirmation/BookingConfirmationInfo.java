package bookingconfirmation;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.ActivityDetails;

public class BookingConfirmationInfo {

	private  WebDriver driver = null; 
	private FirefoxProfile profile;
	JavascriptExecutor javaScriptExe = (JavascriptExecutor) driver;
	ActivityDetails activitydetails 	= new ActivityDetails();
	
	
	public BookingConfirmationInfo(ActivityDetails activityDetails, WebDriver Driver){
		
		this.driver = Driver;
		this.activitydetails = activityDetails;
	}
	
	public WebDriver viewBookingConfirmationReport(String reservationNo, WebDriver Driver) throws InterruptedException{
		
		WebDriverWait wait = new WebDriverWait(driver, 90);	
		
		driver.get(PG_Properties.getProperty("Baseurl")+ "/reports/operational/mainReport.do?reportId=33&reportName=Booking Confirmation Report");
		
		
		Thread.sleep(2000);
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='productType_activities']")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("searchby_cusname")));
		
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='searchby_reservationno']")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationno")));
		
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='actkCon_label_text']")).click();
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(reservationNo);
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("emaillayoutbasedon_label_text")));
		
		
		
		return driver;
	}
	
}
