package com.model;

import java.util.ArrayList;

public class ReservationResponse {

	private String tourDate = "";
	private String currency = "";
	private String grossValue = "";
	private String bookingStatus = "";
	private String city = "";
	private String activityName = "";
	private String supplierConfirmationNo = "";
	private ArrayList<String> childAgesList;
	private String url = "";
	
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getSupplierConfirmationNo() {
		return supplierConfirmationNo;
	}
	public void setSupplierConfirmationNo(String supplierConfirmationNo) {
		this.supplierConfirmationNo = supplierConfirmationNo;
	}
	public ArrayList<String> getChildAgesList() {
		return childAgesList;
	}
	public void setChildAgesList(ArrayList<String> childAgesList) {
		this.childAgesList = childAgesList;
	}
	public String getTourDate() {
		return tourDate;
	}
	public void setTourDate(String tourDate) {
		this.tourDate = tourDate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getGrossValue() {
		return grossValue;
	}
	public void setGrossValue(String grossValue) {
		this.grossValue = grossValue;
	}
	public String getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
	
	
	
}
