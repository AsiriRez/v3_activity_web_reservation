package com.runtest;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import reservationreport.LoadReservations;
import reservationreport.Reservation;
import bookingconfirmation.BookingConfirmation;
import bookingconfirmation.LoadBookingConfirmationDetails;
import bookingconfirmation.Quotation;
import bookinglistreport.BookingList;
import bookinglistreport.LoadBookingDetails;
import cancellation_modification.Cancellation;
import cancellation_modification.CancellationFlow;
import cancellation_modification.Modification;
import cancellation_modification.ModificationFlow;

import com.controller.*;
import com.model.*;
import com.opera.core.systems.OperaDriver;
import com.reader.*;

public class RunTest {
	
	private WebDriver driver         = null;
	private FirefoxProfile profile;
	private LoadDetails loadDetails;
	private ThirdLoadDetails thirdLoadDetails;
	private StringBuffer  PrintWriter;
	private int TestCaseCount;
	private TreeMap<String, Search>   SearchList;
	private TreeMap<String, ThirdSearch>   SearchList_Third;
	
	private int i=0;	
	private ArrayList<Map<Integer, String>> sheetlist, thirdPartySheetlist;	
	private Map<Integer, String> activitySearchInfoMap 		= null;
	private Map<Integer, String> activityPaymentsMap 		= null;
	private Map<Integer, String> activityInventoryMap 		= null;
	
	private Map<Integer, String> activitySearchInfoMap_Third 		= null;
	private Map<Integer, String> activityPaymentsMap_Third 			= null;
	private Map<Integer, String> activityInventoryMap_Third 		= null;
	
	@Before
	public void setUp() throws MalformedURLException{

		if (PG_Properties.getProperty("RunMode").equalsIgnoreCase("Local")) {
			profile 		= new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
			driver 			= new FirefoxDriver(profile);
		
		} else {
			
			DesiredCapabilities capabilities = new DesiredCapabilities();
			
			if (PG_Properties.getProperty("Browser").equalsIgnoreCase("firefox")) {
				
				profile 		= new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
				driver 			= new FirefoxDriver(profile);
			
			}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("chrome")){
				
				System.setProperty("webdriver.chrome.driver","D:\\chromedriver.exe");
				driver = new ChromeDriver();
				  				
			}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("ie")){
				
				try {
					System.setProperty("webdriver.ie.driver","D:\\IEDriverServer.exe");
					driver = new InternetExplorerDriver();
				
				} catch (Exception e) {					
					e.printStackTrace();
				}
					
			}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("safari")){
				
				driver = new SafariDriver();
								
			}else{				
			
				capabilities.setCapability("opera.binary", "C://Program Files (x86)//Opera//opera.exe");
				capabilities.setCapability("opera.log.level", "CONFIG");
				driver = new OperaDriver(capabilities);
				
			}
			
						
			try {
				
				capabilities.setCapability("name", "Selenium Test Example");
				capabilities.setCapability("build", "1.0");
				capabilities.setCapability("browser_api_name", ""+PG_Properties.getProperty("BrowserVersion")+"");
				capabilities.setCapability("os_api_name", ""+PG_Properties.getProperty("OSVersion")+"");
				capabilities.setCapability("screen_resolution", "1024x768");
				capabilities.setCapability("record_video", "true");
				capabilities.setCapability("record_network", "true");
				capabilities.setCapability("record_snapshot", "false");
				
				driver = new RemoteWebDriver(new URL("http://surani%40rezgateway.com:u6e72a2194362230@hub.crossbrowsertesting.com:80/wd/hub"), capabilities);
			
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		sheetlist = new ArrayList<Map<Integer, String>>();
		thirdPartySheetlist = new ArrayList<Map<Integer, String>>();
		PrintWriter = new StringBuffer();
		ExcelReader reader = new ExcelReader();	
		
		sheetlist 	= reader.init(PG_Properties.getProperty("WEB.ExcelPath.ActivityReservation"));
		thirdPartySheetlist = reader.init(PG_Properties.getProperty("Activity.ThirdParty"));
		
		loadDetails = new LoadDetails(sheetlist);
		thirdLoadDetails = new ThirdLoadDetails(thirdPartySheetlist);
	
		activitySearchInfoMap 	= sheetlist.get(0);
		activityPaymentsMap		= sheetlist.get(1);
		activityInventoryMap   	= sheetlist.get(2);
		
		activitySearchInfoMap_Third 	= thirdPartySheetlist.get(0);
		activityPaymentsMap_Third		= thirdPartySheetlist.get(1);
		activityInventoryMap_Third   	= thirdPartySheetlist.get(2);
		
		
				
		try {
			SearchList = loadDetails.loadActivityReservation(activitySearchInfoMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			SearchList = loadDetails.loadPaymentDetails(activityPaymentsMap, SearchList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			SearchList = loadDetails.loadActivityInventoryRecords(activityInventoryMap, SearchList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//
		
		try {
			SearchList_Third = thirdLoadDetails.loadActivityReservation(activitySearchInfoMap_Third);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			SearchList_Third = thirdLoadDetails.loadPaymentDetails(activityPaymentsMap_Third, SearchList_Third);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			SearchList_Third = thirdLoadDetails.loadActivityInventoryRecords(activityInventoryMap_Third, SearchList_Third);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void activityReservatiionFlow() throws InterruptedException, IOException, ParseException{
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime());
		WebReservationFlow webreservationFlow = new WebReservationFlow(driver);
		ThirdWebReservationFlow thirdWebReservationFlow = new ThirdWebReservationFlow(driver);
		RateChecker rateCheck = new RateChecker();
		
		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Hotel Reservation Report - Date ["+currentDateforMatch+"]</p></div>");
		PrintWriter.append("<body>");		
		PrintWriter.append("<br><br>");
		
		TestCaseCount = 1;
		PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th> <th>Test Status</th></tr>");
				
		Iterator<Map.Entry<String, Search>> it = (Iterator<Entry<String, Search>>) SearchList.entrySet().iterator();
		Iterator<Map.Entry<String, ThirdSearch>> it_Third = (Iterator<Entry<String, ThirdSearch>>) SearchList_Third.entrySet().iterator();
		
		PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>System Availability</td>");
		PrintWriter.append("<td>Should Be Loaded</td>");
		
		if (isCCPageLoaded(driver) == true) {
			
			PrintWriter.append("<td>Page Loaded..!!!!</td>");
			PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
			
			//Search scenarios for Internal
			if (PG_Properties.getProperty("ResultsType").equalsIgnoreCase("Internal")) {
								
				while(it.hasNext()){
					
					Search searchInfo  = it.next().getValue();
					webreservationFlow.addSearchScenario(searchInfo);
					Reservation reservationdetails = new Reservation();
					BookingConfirmation confirmationDetails = new BookingConfirmation();
					Modification modDetails = new Modification();
					BookingList bookingList = new BookingList();
					Map<String, String> currencyMap = new HashMap<String, String>();
					Cancellation cDetails = new Cancellation();
					ActivityDetails activityDetails = new ActivityDetails();
					Quotation quotationDetails = new Quotation();
					i++;
							
					try {
						
						TORatesLogics trLogics = new TORatesLogics(searchInfo);	
						ActivityDetails actDetails = trLogics.getTORatesCalculation();	
						driver = webreservationFlow.searchEngine(driver, actDetails);
						activityDetails = webreservationFlow.searchResults(driver);
						
						TestCaseCount++;
						PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Login Status</td>");
						PrintWriter.append("<td>Should login successfully</td>");
										
						if (login(driver) == true){
							
							PrintWriter.append("<td>Success..!!!</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
							
							TestCaseCount++;
							PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Activity WEB Reservations</td>");
							PrintWriter.append("<td>Reservation should be made</td>");
							
//							ModificationFlow mFlow = new ModificationFlow("A5443W100615", driver);					
//							modDetails = mFlow.modificationFlow(driver);
							
//							LoadBookingDetails loadBookingDetails = new LoadBookingDetails("A0042W091015", searchInfo, driver);					
//							bookingList = loadBookingDetails.getBookingCardList(driver);
						
							if (activityDetails.isResultsAvailable() == true) {
								
								if (searchInfo.getQuotationReq().equalsIgnoreCase("Yes")) {
									
									currencyMap = rateCheck.getExchangeRates(driver);
									quotationDetails = webreservationFlow.getQuotationMail(driver);
									
									ReportsReader reportsReader = new ReportsReader(activityDetails, quotationDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails ,driver);				
									reportsReader.getActvivtyInventoryReport(driver);
									reportsReader.getcancellationModificationSummary(driver);
									
									PrintWriter.append("<td>ACTIVITY >> "+searchInfo.getActivityName()+" -> Quotation No ["+activityDetails.getQuote_QuotationNo()+"] :- Done</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");
									
									
								} else {
									
									currencyMap = rateCheck.getExchangeRates(driver);
									
									LoadReservations loadReservations  = new LoadReservations(activityDetails, driver);					
									reservationdetails = loadReservations.getReservationReportDetails(driver);
									
									LoadBookingConfirmationDetails loadConfirmationDetails = new LoadBookingConfirmationDetails(activityDetails, driver);					
									confirmationDetails = loadConfirmationDetails.getConfirmationDetails(driver);
									
									ModificationFlow mFlow = new ModificationFlow(activityDetails, driver);					
									modDetails = mFlow.modificationFlow(driver);
									
									LoadBookingDetails loadBookingDetails = new LoadBookingDetails(activityDetails, searchInfo, driver);					
									bookingList = loadBookingDetails.getBookingCardList(driver);
									
									CancellationFlow cFlow= new CancellationFlow(activityDetails, driver);					
									cDetails = cFlow.cancellationFlow(driver);
									
									ReportsReader reportsReader = new ReportsReader(activityDetails, quotationDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails ,driver);				
									reportsReader.getActvivtyInventoryReport(driver);
									reportsReader.getcancellationModificationSummary(driver);
									
									PrintWriter.append("<td>ACTIVITY >> "+searchInfo.getActivityName()+" -> Reservation No ["+activityDetails.getReservationNo()+"] :- Done</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
									
								}
															
							}else{
								
								ReportsReader reportsReader = new ReportsReader(activityDetails, quotationDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails ,driver);				
								reportsReader.getActvivtyInventoryReport(driver);
								reportsReader.getcancellationModificationSummary(driver);
								
								PrintWriter.append("<td>ACTIVITY >> No Results Available</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
								
							}				
							
						}else{
							
							PrintWriter.append("<td>Login Errorrr..!!!!</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					
					
					} catch (Exception e) {
						e.printStackTrace();
				        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
				        
						PrintWriter.append("<td>Error :- [["+e.toString()+"]]</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
						
						ReportsReader reportsReader = new ReportsReader(activityDetails, quotationDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails ,driver);				
						reportsReader.getActvivtyInventoryReport(driver);
						reportsReader.getcancellationModificationSummary(driver);
						
						
					}	
				}
				
			}
			
			/*
			//Search scenarios for ThirdParty
			if (PG_Properties.getProperty("ResultsType").equalsIgnoreCase("ThirdParty")) {
								
				while(it_Third.hasNext()){
					
					ThirdSearch searchInfo  = it_Third.next().getValue();
					thirdWebReservationFlow.addSearchScenario(searchInfo);					
					i++;
							
					try {
						
						Reservation reservationdetails = new Reservation();
						BookingConfirmation confirmationDetails = new BookingConfirmation();
						Modification modDetails = new Modification();
						BookingList bookingList = new BookingList();
						Map<String, String> currencyMap = new HashMap<String, String>();
						Cancellation cDetails = new Cancellation();
						CancellationPolicyResponse cancelPolicy_Response = new CancellationPolicyResponse();
						
						driver = thirdWebReservationFlow.searchEngine(driver);
						ActivityDetails activityDetails = thirdWebReservationFlow.searchResults(driver);
						
						TestCaseCount++;
						PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Login Status</td>");
						PrintWriter.append("<td>Should login successfully</td>");
										
						if (login(driver) == true){
							
							PrintWriter.append("<td>Success..!!!</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
							
							TestCaseCount++;
							PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Activity WEB Reservations</td>");
							PrintWriter.append("<td>Reservation should be made</td>");
						
							if (activityDetails.isResultsAvailable() == true) {
								
								currencyMap = rateCheck.getExchangeRates(driver);
								
								LoadReservations loadReservations  = new LoadReservations(activityDetails, driver);					
								reservationdetails = loadReservations.getReservationReportDetails(driver);
								
								LoadBookingConfirmationDetails loadConfirmationDetails = new LoadBookingConfirmationDetails(activityDetails, driver);					
								confirmationDetails = loadConfirmationDetails.getConfirmationDetails(driver);
								
								ModificationFlow mFlow = new ModificationFlow(activityDetails, driver);					
								modDetails = mFlow.modificationFlow(driver);
								
								LoadBookingDetails loadBookingDetails = new LoadBookingDetails(activityDetails, driver);					
								bookingList = loadBookingDetails.getBookingCardList(driver);
								
								CancellationFlow cFlow= new CancellationFlow(activityDetails, driver);					
								cDetails = cFlow.cancellationFlow(driver);
																								
								if (PG_Properties.getProperty("SupplierType").equals("GTA")) {
									
									GTAReader gtaReader = new GTAReader(activityDetails, searchInfo, currencyMap, driver);
									gtaReader.readAvailabilityRequests();
									gtaReader.readAvailabilityResponses();
									gtaReader.getGTAAvailabilityReport(searchInfo , driver);
									
									gtaReader.readProgramRequest();
									gtaReader.readProgramResponse();		
									gtaReader.getGTAProgramReport(searchInfo, driver);
									
									gtaReader.readCancellationPolicyRequests();
									cancelPolicy_Response = gtaReader.readCancellationPolicyResponse();
									gtaReader.getGTACancellationPolicyReport(searchInfo, driver);
									
									gtaReader.readReservationRequest();
									gtaReader.readReservationResponse();
									gtaReader.getGTAReservationReport(searchInfo, driver);
																			
								}
								
								ThirdReportsReader reportsReader = new ThirdReportsReader(activityDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails , cancelPolicy_Response, driver);				
								reportsReader.getActvivtyInventoryReport(driver);
								reportsReader.getcancellationModificationSummary(driver);
								
								PrintWriter.append("<td>ACTIVITY >> "+searchInfo.getActivityName()+" -> Reservation No ["+activityDetails.getReservationNo()+"] :- Done</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
								
								
								
															
							}else{
								
								ThirdReportsReader reportsReader = new ThirdReportsReader(activityDetails, currencyMap, searchInfo, bookingList, confirmationDetails, reservationdetails, cDetails, modDetails , cancelPolicy_Response, driver);				
								reportsReader.getActvivtyInventoryReport(driver);
								reportsReader.getcancellationModificationSummary(driver);
								
								PrintWriter.append("<td>ACTIVITY >> No Results Available</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
								
							}					
							
						}else{
							
							PrintWriter.append("<td>Login Errorrr..!!!!</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
					
					} catch (Exception e) {
						e.printStackTrace();
				        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
				        
						PrintWriter.append("<td>Error :- [["+e.toString()+"]]</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
						
					}	
				}
				
			}
			*/
			
					
		}else{
			
			PrintWriter.append("<td>Page is Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");			
		}
		
		
		
		
		PrintWriter.append("</table>");
	}
	
	@After
	public void tearDown() throws IOException{
		
		PrintWriter.append("</body></html>");
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("ActivityInternalReport"))));
		bwr.write(PrintWriter.toString());
		bwr.flush();
		bwr.close();
	    driver.quit();
		
	}
	
	public boolean isCCPageLoaded(WebDriver driver_1) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
		Thread.sleep(1500);
		driver.switchTo().defaultContent();
		
		try {			
			WebElement idElement = driver.findElement(By.id("user_id"));
			idElement.isDisplayed();
			return true;
			
		} catch (Exception e) {
			return false;
		}		
	}
	
	public boolean login(WebDriver Driver){

		this.driver = Driver;
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("user_id")));
		driver.findElement(By.id("user_id")).clear();
		driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty("UserName"));
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty("Password"));
		driver.findElement(By.xpath(".//*[@id='loginbutton']/img")).click();
		
		try {			
			driver.findElement(By.id("mainmenurezglogo"));
			return true;
		} catch (Exception e) {
			System.out.println("Login Fail...");
			return false;
		}		
	}

}
