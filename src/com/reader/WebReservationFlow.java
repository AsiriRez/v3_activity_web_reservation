package com.reader;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bookingconfirmation.Quotation;

import com.controller.PG_Properties;
import com.model.*;
import com.runtest.RunTest;
import com.types.TourOperatorType;

public class WebReservationFlow {

	private WebDriver driver = null;
	private Search searchInfo;
	private int childCountFromexcel;
	private int divCount;
	private List<String> valueList;
	private String currentDate, currentDateforImages;
	private String idOnly, IdStatus, idNo2, idForRemove, idNoFor2, selectedDateExcel, idOnlyForPickup, onLineTp_2, cCountry;
	private ActivityDetails activityDetails;
	private long resultsAvailable;
	private long searchButtClick;
	private long loadPayment, addedtoCart, availablClick;
	private long comfirmBooking, firstSearch, firstResultsAvail;
	private String ToActivityPM, paymentType, discountYesNo, ToActivityNetRate, ToActivity_SalesTaxType, ToActivity_SalesTaxValue, ToActivity_MisFeeType, ToActivity_MisFeeValue;
	private TourOperatorType UserTypeDC_B2B;
	

	public WebReservationFlow(WebDriver Driver) {

		this.driver = Driver;
	}

	public void addSearchScenario(Search searchInfoDe) {

		this.searchInfo = searchInfoDe;
	}

	public WebDriver searchEngine(WebDriver webdriver, ActivityDetails activityDetails2) throws InterruptedException, ParseException, IOException {

		this.activityDetails = activityDetails2;
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());

		WebDriverWait wait = new WebDriverWait(driver, 120);
		valueList = new ArrayList<String>();

		ArrayList<ActivityInfo> activityList = searchInfo.getInventoryInfo();

		for (ActivityInfo activityInfo : activityList) {

			ToActivityPM = activityInfo.getToActivityPM();
			ToActivityNetRate = activityInfo.getToActivityNetRate();
			ToActivity_SalesTaxType = activityInfo.getToActivity_SalesTaxType();
			ToActivity_SalesTaxValue = activityInfo.getToActivity_SalesTaxValue();
			ToActivity_MisFeeType = activityInfo.getToActivity_MisFeeType();
			ToActivity_MisFeeValue = activityInfo.getToActivity_MisFeeValue();
			UserTypeDC_B2B = activityInfo.getUserTypeDC_B2B();
			discountYesNo = activityInfo.getDiscountYesORNo();
			paymentType = activityInfo.getPaymentType();

		}

		if (UserTypeDC_B2B == TourOperatorType.DC) {

			/*driver.get(PG_Properties.getProperty("Activity_Temp5"));
			Thread.sleep(2500);*/
			
			driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
			Thread.sleep(1500);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("user_id")));

			driver.findElement(By.id("user_id")).clear();
			driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty("UserName"));
			driver.findElement(By.id("password")).clear();
			driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty("Password"));
			driver.findElement(By.xpath(".//*[@id='loginbutton']/img")).click();
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/table/tbody/tr[1]/td/table/tbody/tr/td[5]/img")));
			driver.findElement(By.xpath(".//*[@id='main_mnu_0']")).click();
			driver.findElement(By.xpath(".//*[@id='mnu_8']")).click();
			
			
			

		} else {

			driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
			Thread.sleep(1500);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("user_id")));

			driver.findElement(By.id("user_id")).clear();
			driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[0]);
			driver.findElement(By.id("password")).clear();
			driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty(UserTypeDC_B2B.toString()).split("/")[1]);
			driver.findElement(By.xpath(".//*[@id='loginbutton']/img")).click();

			if (UserTypeDC_B2B == TourOperatorType.NETCASH) {

				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='partnermenuitems']/table[3]/tbody/tr[12]/td[2]/a")));
				driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[3]/tbody/tr[2]/td[2]/a")).click();
				//driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[3]/tbody/tr[12]/td[2]/a")).click();
			}

			if (UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY) {

				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[12]/td[2]/a")));
				driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[2]/td[2]/a")).click();
				//driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[12]/td[2]/a")).click();
			}

			if (UserTypeDC_B2B == TourOperatorType.NETCREDITLPON) {

				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[12]/td[2]/a")));
				driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[2]/td[2]/a")).click();
				//driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[12]/td[2]/a")).click();
			}
			
			if (UserTypeDC_B2B == TourOperatorType.COMCASH) {

				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='partnermenuitems']/table[3]/tbody/tr[12]/td[2]/a")));
				driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[3]/tbody/tr[2]/td[2]/a")).click();
				//driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[3]/tbody/tr[12]/td[2]/a")).click();
				
			}

			if (UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {

				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[12]/td[2]/a")));
				driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[6]/td[2]/a")).click();
				//driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[12]/td[2]/a")).click();
			}
			
			if (UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO) {

				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[12]/td[2]/a")));
				driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[2]/td[2]/a")).click();
				//driver.findElement(By.xpath(".//*[@id='partnermenuitems']/table[4]/tbody/tr[12]/td[2]/a")).click();
			}

		}

		driver.switchTo().defaultContent();
		driver.switchTo().frame("bec_container_frame");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hLi")));

		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
		Thread.sleep(1500);

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_BookingEngine.png"));

		if (driver.findElements(By.id("AC_Country")).size() == 0) {
			activityDetails.setBookingEngineLoaded(false);
		}

		// ///

		if (driver.findElement(By.id("AC_Country")).isDisplayed() == false) {
			activityDetails.setCountryLoaded(false);
		}
		if (driver.findElement(By.id("activity_Loc")).isDisplayed() == false) {
			activityDetails.setCityLoaded(false);
		}
		if (driver.findElement(By.id("ac_departure_temp")).isDisplayed() == false) {
			activityDetails.setDateFromLoaded(false);
		}
		if (driver.findElement(By.id("ac_arrival_temp")).isDisplayed() == false) {
			activityDetails.setDateToLoaded(false);
		}
		if (driver.findElement(By.id("R1occAdults_A")).isDisplayed() == false) {
			activityDetails.setAdultsLoaded(false);
		}
		if (driver.findElement(By.id("R1occChildren_A")).isDisplayed() == false) {
			activityDetails.setChildLoaded(false);
		}

		if (!(driver.findElement(By.xpath(".//*[@id='AC_Country']/option[3]")).getText().equalsIgnoreCase("Albania"))) {
			activityDetails.setCountryListLoaded(false);
		}

		Thread.sleep(5500);
		driver.findElement(By.id("ac_departure_temp")).click();
		File scrFile22 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile22, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_BookingEngine_CalenderAvailable.png"));
		if (driver.findElement(By.id("ui-datepicker-div")).isDisplayed() == false) {
			activityDetails.setCalenderAvailble(false);
		}

		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
		Thread.sleep(1500);

		String showAdd = driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).getText();
		activityDetails.setShowAdd(showAdd);

		driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).click();
		Thread.sleep(500);

		File scrFile23 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile23, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_BookingEngine_AdditionalSearchOptions.png"));
		Thread.sleep(500);

		if (driver.findElement(By.id("activity_type")).isDisplayed() == false) {
			activityDetails.setActivityType(false);
		}
		if (driver.findElement(By.id("activityTypeId_a")).isDisplayed() == false) {
			activityDetails.setProgramCat(false);
		}
		if (driver.findElement(By.id("AC_consumerCurrencyCode")).isDisplayed() == false) {
			activityDetails.setPreCurrency(false);
		}
		if (driver.findElement(By.id("discountCoupon_No_A")).isDisplayed() == false) {
			activityDetails.setPromoCode(false);
		}

		if (!(driver.findElement(By.xpath(".//*[@id='AC_consumerCurrencyCode']/option[2]")).getText().equalsIgnoreCase("AED"))) {
			activityDetails.setCurrencyListLoaded(false);
		}

		if (!(driver.findElement(By.xpath(".//*[@id='activityTypeId_a']/option[2]")).getText().equalsIgnoreCase("Outdoor Activities"))) {
			activityDetails.setProgCatListLoaded(false);
		}

		String hideAdd = driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).getText();
		activityDetails.setHideAdd(hideAdd);

		driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).click();
		Thread.sleep(500);

		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
		Thread.sleep(1500);

		// //

		try {
			new Select(driver.findElement(By.id("AC_Country"))).selectByVisibleText(searchInfo.getCountry());
		} catch (Exception e) {

			e.printStackTrace();
		}

		String hiddenDest = searchInfo.getDestination();
		driver.findElement(By.id("activity_Loc")).sendKeys(hiddenDest);
		Thread.sleep(2000);
		driver.findElement(By.partialLinkText(hiddenDest)).click();

		((JavascriptExecutor) driver).executeScript("$('#ac_departure').val('" + searchInfo.getDateFrom() + "');");
		((JavascriptExecutor) driver).executeScript("$('#ac_arrival').val('" + searchInfo.getDateTo() + "');");

		new Select(driver.findElement(By.id("R1occAdults_A"))).selectByVisibleText(searchInfo.getAdults());
		new Select(driver.findElement(By.id("R1occChildren_A"))).selectByVisibleText(searchInfo.getChildren());

		childCountFromexcel = Integer.parseInt(searchInfo.getChildren());

		if (searchInfo.getAgeOfChildren().contains("#")) {

			String ages = searchInfo.getAgeOfChildren();
			String[] partsEA = ages.split("#");
			for (String value : partsEA) {
				valueList.add(value);
			}
		}

		else {
			String ages = searchInfo.getAgeOfChildren();
			valueList.add(ages);
		}

		for (int i = 0; i < childCountFromexcel; i++) {

			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("A_R1childage_" + (i + 1) + ""))).selectByVisibleText(valueList.get(i));
		}

		((JavascriptExecutor) driver).executeScript("search('A');");

		/*
		 * WebElement element = driver.findElement(By.id("search_btns_h")); JavascriptExecutor executor = (JavascriptExecutor)driver; executor.executeScript("arguments[0].click();", element);
		 */

		// ((JavascriptExecutor)driver).executeScript("return validate('formA');");
		// driver.findElement(By.id("search_btns_h")).click();
		firstSearch = (System.currentTimeMillis() / 1000);

		// ((JavascriptExecutor)driver).executeScript("$('#search_btns_h').click();");

		Thread.sleep(3000);

		return driver;
	}

	
	public ActivityDetails searchResults(WebDriver webDriver) throws InterruptedException, IOException {

		this.driver = webDriver;

		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		WebDriverWait wait = new WebDriverWait(driver, 200);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")));

		activityDetails.setTimeFirstSearch(firstSearch);
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
		currentDate = format1.format(cal.getTime());

		SimpleDateFormat formatToDate = new SimpleDateFormat("dd-MMM-yyyy");
		String cDate = formatToDate.format(cal1.getTime());
		activityDetails.setCurrentDate(cDate);
		activityDetails.setScenarioCount(searchInfo.getScenarioCount());

		driver.switchTo().defaultContent();

		Thread.sleep(20500);
		String showResAdd = driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).getText();
		activityDetails.setShowResultsAdd(showResAdd);

		driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
		Thread.sleep(500);

		File scrFile231 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile231, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_ResultsPage_AdditionalFilters.png"));
		Thread.sleep(500);

		if (driver.findElement(By.className("activity-prog-category")).isDisplayed() == false) {
			activityDetails.setResultsProgramCat(false);
		}
		if (driver.findElement(By.id("AC_consumerCurrencyCode")).isDisplayed() == false) {
			activityDetails.setResultsPreCurrency(false);
		}
		if (driver.findElement(By.id("discountCoupon_No_H")).isDisplayed() == false) {
			activityDetails.setResultsPromoCode(false);
		}
		if (!(driver.findElement(By.xpath(".//*[@id='AC_consumerCurrencyCode']/option[2]")).getText().equalsIgnoreCase("AED"))) {
			activityDetails.setResultsCurrencyListLoaded(false);
		}
		if (!(driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[8]/ul/li[1]/div/select/option[2]")).getText().equalsIgnoreCase("Outdoor Activities"))) {
			activityDetails.setResultsProgCatListLoaded(false);
		}

		String hideResAdd = driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).getText();
		activityDetails.setHideResultsAdd(hideResAdd);

		driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
		Thread.sleep(500);

		if (driver.findElement(By.id("price_slider_WJ_5")).isDisplayed() == false) {
			activityDetails.setPriceRangeFilter(false);
		}

		if (driver.findElement(By.id("booktype_text_WJ_8")).isDisplayed() == false) {
			activityDetails.setProgramAvailablity(false);
		}

		if (driver.findElement(By.id("name_text_WJ_4")).isDisplayed() == false) {
			activityDetails.setActivityNameFilter(false);
		}

		// ////////

		boolean resultsElement;

		if (driver.findElements(By.id("activity_results_container_0")).size() != 0) {
			resultsElement = true;
			activityDetails.setResultsAvailable(resultsElement);
			firstResultsAvail = (System.currentTimeMillis() / 1000);
			activityDetails.setTimeFirstResults(firstResultsAvail);
		} else {
			resultsElement = false;
			activityDetails.setResultsAvailable(resultsElement);
		}

		File scrFile_2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile_2, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_ResultsPage.png"));

		if (resultsElement == true) {

			int activityCount = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='content']/div/div/div[1]/div/div/p[2]/span")).getText());
			System.out.println("Activity count - " + activityCount);
			activityDetails.setActivityCount(activityCount);

			int wholepages = (activityCount / 10);

			if (((activityCount) % 10) == 0) {
				System.out.println("Page count - " + wholepages);
			} else {
				wholepages++;
				System.out.println("Page count - " + wholepages);
			}

			// Activity filter checking
			// Slider

			try {
				String minValue = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[2];
				String maxValue = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[6];
				activityDetails.setMinPrize(minValue);
				activityDetails.setMaxPrize(maxValue);
			} catch (Exception e1) {
				activityDetails.setMinPrize("Not Available");
				activityDetails.setMaxPrize("Not Available");
			}

			// 1
			String pageLoadedPrize = driver.findElement(By.xpath(".//*[@id='activity_results_container_0']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
			activityDetails.setPageLoadedResults(pageLoadedPrize);

			// 2

			WebElement slider = driver.findElement(By.xpath(".//*[@id='price_slider_WJ_5']/a[1]"));
			Actions move = new Actions(driver);
			Action action = move.dragAndDropBy(slider, 10, 0).build();
			action.perform();
			Thread.sleep(1000);

			String prizeChanged = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[2];
			activityDetails.setPrizeChanged_1(prizeChanged);
			String resultsPrize = driver.findElement(By.xpath(".//*[@id='activity_results_container_0']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
			activityDetails.setPrizeResults_1(resultsPrize);

			// 3
			Actions move2 = new Actions(driver);
			Action action_2 = move2.dragAndDropBy(slider, 200, 0).build();
			action_2.perform();

			String prizeChanged_100 = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[2];
			activityDetails.setPrizeChanged_2(prizeChanged_100);
			String resultsPrize_100 = driver.findElement(By.xpath(".//*[@id='activity_results_container_0']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
			activityDetails.setPrizeResults_2(resultsPrize_100);
			
			
			//Reset Filter
			Thread.sleep(1500);
			driver.findElement(By.xpath(".//*[@id='content-l']/div[2]/div[5]/a")).click();
			Thread.sleep(2500);

			// On req

			Thread.sleep(2500);
			new Select(driver.findElement(By.xpath(".//*[@id='booktype_text_WJ_8']"))).selectByValue("O");
			Thread.sleep(2500);

			ArrayList<String> onReqLab = new ArrayList<String>();
			if (driver.findElements(By.className("result-block")).size() != 0) {

				List<WebElement> resultsblock = driver.findElements(By.className("result-block"));

				activityDetails.setResulteBlockSizeOnReq(resultsblock.size());

				for (int i = 0; i < resultsblock.size(); i++) {

					String labeltext = driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[1]/div[2]/ul/li/p")).getText();
					onReqLab.add(labeltext);
				}

				activityDetails.setOnRequestLabels(onReqLab);

			} else {

				activityDetails.setResulteBlockSizeOnReq(1);
				onReqLab.add("On Request Activities Not Available");
				activityDetails.setOnRequestLabels(onReqLab);

			}

			// ////

			Random ran = new Random();
			int x = ran.nextInt(100) + 5;

			if (x % 2 == 0) {
				activityDetails.setAddAndContinue("true");
			} else {
				activityDetails.setAddAndContinue("false");
			}

			selectedDateExcel = searchInfo.getActivityDate();

			// select All activities
			Thread.sleep(3500);
			new Select(driver.findElement(By.xpath(".//*[@id='booktype_text_WJ_8']"))).selectByValue("B");
			Thread.sleep(2500);

			// /

			ArrayList<Integer> prizeList = new ArrayList<Integer>();
			int prizeForList;
			
			if (10 <= activityCount) {

				ArrayList<Integer> prizeListEqual = new ArrayList<Integer>();

				for (int j = 0; j < 10; j++) {

					Thread.sleep(1500);
					prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_" + j + "']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeList.add(prizeForList);

					if (j == 0) {

						if (activityDetails.getAddAndContinue().equals("true")) {

							Thread.sleep(1500);
							WebElement mainElement = driver.findElement(By.id("activity_results_container_" + j + ""));
							WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
							idForRemove = idElement.getAttribute("code");
							Thread.sleep(1500);

							new Select(driver.findElement(By.xpath(".//*[@id='" + idForRemove + "_date_select']"))).selectByVisibleText(selectedDateExcel);
							Thread.sleep(1500);

							driver.findElement(By.xpath(".//*[@id='activity_results_container_" + j + "']/div/div[1]/div[2]/ul/li[3]/a")).click();
							Thread.sleep(3500);

							WebElement selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idForRemove + "']/div[2]/div[4]"));
							idNoFor2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];

							// checkbox
							Thread.sleep(1000);
							driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_" + idForRemove + "_" + idNoFor2 + "']")).click();
							Thread.sleep(1000);
							
							WebElement elementChangeID = driver.findElement(By.id("activity_content_WJ_1_" + idForRemove + ""));
							List<WebElement> classElements = elementChangeID.findElements(By.className("activity-list-body"));

							// Add and Continue

							driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idForRemove + "']/div[" + (classElements.size() + 2) + "]/div/a[2]")).click();
							Thread.sleep(5500);
							driver.findElement(By.xpath("html/body/div[7]/span/i")).click();
							Thread.sleep(3000);

							File scrFile_6 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile_6, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_AddToCart.png"));

							if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setAddToCart(false);
							}

							driver.findElement(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).click();
							Thread.sleep(3000);

							if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setActivityRemoved(false);
							}

							driver.findElement(By.xpath("html/body/div[11]/div[11]/div/button[1]")).click();
							Thread.sleep(3000);

							File scrFile_7 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile_7, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_RemoveCart.png"));

							driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[5]/input")).click();
							Thread.sleep(3000);

						}
					}
				}

				prizeListEqual.addAll(prizeList);
				activityDetails.setBeforeSorting(prizeListEqual);

			} else {
				ArrayList<Integer> prizeListEqual = new ArrayList<Integer>();

				for (int j = 0; j < activityCount; j++) {

					prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_" + j + "']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeList.add(prizeForList);

					if (j == 0) {

						if (activityDetails.getAddAndContinue().equals("true")) {

							WebElement mainElement = driver.findElement(By.id("activity_results_container_" + j + ""));
							WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
							idForRemove = idElement.getAttribute("code");

							new Select(driver.findElement(By.xpath(".//*[@id='" + idForRemove + "_date_select']"))).selectByVisibleText(selectedDateExcel);
							Thread.sleep(1500);

							driver.findElement(By.xpath(".//*[@id='activity_results_container_" + j + "']/div/div[1]/div[2]/ul/li[3]/a")).click();
							Thread.sleep(3500);

							WebElement selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idForRemove + "']/div[2]/div[4]"));
							idNoFor2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];

							// checkbox
							Thread.sleep(1000);
							driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_" + idForRemove + "_" + idNoFor2 + "']")).click();
							Thread.sleep(1000);
							
							WebElement elementChangeID = driver.findElement(By.id("activity_content_WJ_1_" + idForRemove + ""));
							List<WebElement> classElements = elementChangeID.findElements(By.className("activity-list-body"));

							// Add and Continue

							driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idForRemove + "']/div[" + (classElements.size() + 2) + "]/div/a[2]")).click();
							Thread.sleep(5500);
							driver.findElement(By.xpath("html/body/div[7]/span/i")).click();
							Thread.sleep(3000);

							File scrFile_6 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile_6, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_AddToCart.png"));

							if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setAddToCart(false);
							}

							driver.findElement(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).click();
							Thread.sleep(3000);

							if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setActivityRemoved(false);
							}

							driver.findElement(By.xpath("html/body/div[11]/div[11]/div/button[1]")).click();
							Thread.sleep(3000);

							File scrFile_7 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile_7, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_RemoveCart.png"));

							driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[5]/input")).click();
							Thread.sleep(3000);

						}
					}
				}

				prizeListEqual.addAll(prizeList);
				activityDetails.setBeforeSorting(prizeListEqual);

			}

			Collections.sort(prizeList);
			activityDetails.setAfterSorting(prizeList);
			
			

			// //////

			// Price highest to lowest
			driver.findElement(By.xpath(".//*[@id='content']/div/div/div[2]/div/div/ul[1]/li/a[2]/i")).click();
			Thread.sleep(1000);
			ArrayList<Integer> prizeListDescendingOrder = new ArrayList<Integer>();
			ArrayList<Integer> prizeListDescendingOrderEQUAL = new ArrayList<Integer>();

			if (10 <= activityCount) {

				for (int j = 0; j < 10; j++) {

					int prizeForList2 = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_" + j + "']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeListDescendingOrder.add(prizeForList2);

				}
			} else {

				for (int j = 0; j < activityCount; j++) {

					int prizeForList2 = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_" + j + "']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeListDescendingOrder.add(prizeForList2);

				}
			}

			prizeListDescendingOrderEQUAL.addAll(prizeListDescendingOrder);
			activityDetails.setBeforeSortingDescending(prizeListDescendingOrderEQUAL);

			Collections.sort(prizeListDescendingOrder, Collections.reverseOrder());
			activityDetails.setAfterSortingDescending(prizeListDescendingOrder);

			driver.findElement(By.xpath(".//*[@id='content']/div/div/div[2]/div/div/ul[1]/li/a[1]/i")).click();
			Thread.sleep(5000);

			//Search again 
			
			driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[5]/input")).click();
			Thread.sleep(5000);
			
			// /////

			final short ACTIVITY_PER_PAGE = 10;
			outerloop: for (int actvityPage = 1; actvityPage <= wholepages; actvityPage++) {
				for (int i = 0; i < ACTIVITY_PER_PAGE; i++) {

					if (((actvityPage - 1) * ACTIVITY_PER_PAGE) + (i) >= activityCount) {

						break outerloop;

					} else {

						String excelActivityName = searchInfo.getActivityName().replaceAll(" ", "");
						String webActivityName = driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[1]/div[2]/div[1]/h1")).getText().replaceAll(" ", "");

						if (excelActivityName.equalsIgnoreCase(webActivityName)) {

							WebElement mainElement = driver.findElement(By.id("activity_results_container_" + i + ""));
							WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
							idOnly = idElement.getAttribute("code");

							WebElement daysElement = driver.findElement(By.id("" + idOnly + "_date_select"));
							List<WebElement> daysCountList = daysElement.findElements(By.tagName("option"));

							new Select(driver.findElement(By.xpath(".//*[@id='" + idOnly + "_date_select']"))).selectByVisibleText(selectedDateExcel);

							activityDetails.setDaysCount((daysCountList.size()) - 1);

							// Currency and Rate
							String activityCurr = driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[2]/ul/li[1]/p/span[1]")).getText();
							activityDetails.setActivityCurrency(activityCurr);

							String mainRate = driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
							activityDetails.setMainRate(mainRate);
							Thread.sleep(500);

							String description = driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[1]/div[2]/div[1]/div")).getText();
							activityDetails.setActDescription(description);

							// more details
							driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();
							Thread.sleep(3000);

							File scrFile_55 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile_55, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_MoreDetails.png"));

							driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();
							Thread.sleep(3000);

							// cancellation policy link

							driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[1]/div[2]/div[2]/a[2]")).click();
							Thread.sleep(3000);

							WebElement cancelElement = driver.findElement(By.className("cancellation-policy-body"));
							List<WebElement> rowsTo = cancelElement.findElements(By.tagName("li"));

							File scrFile_5 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile_5, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_CancellationPolicy.png"));

							ArrayList<String> policyList = new ArrayList<String>();

							for (int j = 1; j <= rowsTo.size() - 1; j++) {

								String policy = driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/section[2]/div/div/ul/li[" + j + "]")).getText();
								policyList.add(policy);

							}

							System.out.println(policyList);
							activityDetails.setCancelPolicy(policyList);

							driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/section[2]/a/i")).click();
							Thread.sleep(1500);

							// ////

							driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[2]/ul/li[3]/a")).click();
							Thread.sleep(3500);

							File scrFile_9 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile_9, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_AvailabilityActivityTypes.png"));

							ArrayList<String> excelActivityTypes = new ArrayList<String>();

							ArrayList<ActivityInfo> activityInfoList = searchInfo.getInventoryInfo();

							for (ActivityInfo activityInfo : activityInfoList) {

								String excelActString = activityInfo.getActivityType();
								excelActivityTypes.add(excelActString);
							}

							List<WebElement> activityTypesElement = driver.findElements(By.className("activity-list-body"));
							divCount = activityTypesElement.size();

							ArrayList<String> resultsPage_ActivityTypes = new ArrayList<String>();
							ArrayList<String> resultsPage_City = new ArrayList<String>();
							ArrayList<String> resultsPage_Period = new ArrayList<String>();
							ArrayList<String> resultsPage_RateType = new ArrayList<String>();
							ArrayList<String> resultsPage_DailyRate = new ArrayList<String>();

							for (int j = 0; j < excelActivityTypes.size(); j++) {

								String excelActivityTypeName = excelActivityTypes.get(j).replaceAll(" ", "");

								for (int k = 1; k <= activityTypesElement.size(); k++) {

									String webActivityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[1]")).getText().replaceAll(" ", "");

									if (excelActivityTypeName.equalsIgnoreCase(webActivityTypeName)) {

										String rs_activityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[1]")).getText();
										resultsPage_ActivityTypes.add(rs_activityTypeName);

										String rs_activityPeriod = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[2]")).getText();
										resultsPage_Period.add(rs_activityPeriod);

										String rs_activityRate = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[3]")).getText();
										resultsPage_RateType.add(rs_activityRate);

										String rs_activityDailyRate = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[5]")).getText();
										resultsPage_DailyRate.add(rs_activityDailyRate);

										WebElement selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[4]"));
										idNo2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];

										int totalPaxCount = Integer.parseInt(searchInfo.getAdults()) + Integer.parseInt(searchInfo.getChildren());
										new Select(driver.findElement(By.xpath(".//*[@id='activity_qty_select_WJ_1_" + idOnly + "_" + idNo2 + "']"))).selectByVisibleText(Integer.toString(totalPaxCount));
										
										Thread.sleep(1000);
										driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_" + idOnly + "_" + idNo2 + "']")).click();
										Thread.sleep(1000);
										
									}
								}
							}

							loadPayment = (System.currentTimeMillis() / 1000);
							activityDetails.setTimeLoadpayment(loadPayment);

							activityDetails.setResultsPage_ActivityTypes(resultsPage_ActivityTypes);
							activityDetails.setResultsPage_Period(resultsPage_Period);
							activityDetails.setResultsPage_RateType(resultsPage_RateType);
							activityDetails.setResultsPage_DailyRate(resultsPage_DailyRate);

							if (activityDetails.getAddAndContinue().equals("true")) {

								// Add and Continue

								driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (divCount + 2) + "]/div/a[2]")).click();
								Thread.sleep(5500);
								addedtoCart = (System.currentTimeMillis() / 1000);
								activityDetails.setTimeAddedtoCart(addedtoCart);
								driver.findElement(By.xpath("html/body/div[7]/span/i")).click();
								Thread.sleep(2000);

								String cartCurrencyandValue = driver.findElement(By.xpath(".//*[@id='basketPrice']/div")).getText();
								activityDetails.setCartVallue(cartCurrencyandValue);

								File scrFile_23 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
								FileUtils.copyFile(scrFile_23, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_AddandContinue.png"));

								Thread.sleep(2000);
								driver.findElement(By.xpath(".//*[@id='basketPrice']/a")).click();
								Thread.sleep(2000);

							} else {

								// Book

								driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (divCount + 2) + "]/div/a[1]")).click();
								addedtoCart = (System.currentTimeMillis() / 1000);
								activityDetails.setTimeAddedtoCart(addedtoCart);
								Thread.sleep(3000);

							}

							break outerloop;

						}
					}
				}

				if (2 <= actvityPage) {
					driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span[" + (actvityPage + 2) + "]/a")).click();
				} else {
					driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span[" + (actvityPage + 1) + "]/a")).click();
				}

			}

			// Payment Page
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("payment-cart-sum-wrapper")));

			WebElement idStatusElement = driver.findElement(By.xpath("/html/body/section/form/div[1]/header/div/div/div[6]/div/div[2]/div/div[1]/ul[2]/li[2]/div"));
			System.out.println(idStatusElement.getText());
			IdStatus = idStatusElement.getAttribute("id").substring(14, 18);

			File scrFile_24 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile_24, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_PaymentsPage.png"));

			// Shopping Basket

			activityDetails.setPaymentDetails("Pay Online");
			activityDetails.setUserType(UserTypeDC_B2B);

			String shoppingCart_CartCurrency = driver.findElement(By.xpath(".//*[@id='sellingcurrency']")).getText();
			activityDetails.setShoppingCart_CartCurrency(shoppingCart_CartCurrency);

			// traveller details
			ArrayList<PaymentDetails> paymentInfoList = searchInfo.getPaymentDetailsInfo();

			for (PaymentDetails paymentDetails : paymentInfoList) {

				new Select(driver.findElement(By.id("cusTitle"))).selectByVisibleText(paymentDetails.getCustomerTitle());
				activityDetails.setPaymentPage_Title(paymentDetails.getCustomerTitle());

				if (!(UserTypeDC_B2B == TourOperatorType.DC)) {

					driver.findElement(By.id("cusFName")).sendKeys(paymentDetails.getCustomerName());
					driver.findElement(By.id("cusLName")).sendKeys(paymentDetails.getCustomerLastName());

					activityDetails.setPaymentPage_FName(paymentDetails.getCustomerName());
					activityDetails.setPaymentPage_LName(paymentDetails.getCustomerLastName());

					String addTo = driver.findElement(By.id("cusAdd1")).getAttribute("value");
					activityDetails.setPaymentPage_address(addTo);

					String tp1 = driver.findElement(By.id("cusareacodetext")).getAttribute("value");
					onLineTp_2 = driver.findElement(By.id("cusPhonetext")).getAttribute("value");
					String tpp = tp1 + "-" + onLineTp_2;
					activityDetails.setPaymentPage_TP(tpp);

					String email = driver.findElement(By.id("cusEmail")).getAttribute("value");
					activityDetails.setPaymentPage_Email(email);

					Select Page_country = new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("cusCountry")));
					WebElement Page_countryw = Page_country.getFirstSelectedOption();
					String cCountryd = Page_countryw.getText();
					activityDetails.setPaymentPage_country(cCountryd);

					String city = driver.findElement(By.id("cusCity")).getAttribute("value");
					activityDetails.setPaymentPage_city(city);

					activityDetails.setPaymentPage_State(paymentDetails.getState());
					activityDetails.setPaymentPage_PostalCode(paymentDetails.getPostalCode());
					cCountry = cCountryd;

				}

				if (UserTypeDC_B2B == TourOperatorType.DC) {

					activityDetails.setPaymentPage_FName(paymentDetails.getCustomerName());
					activityDetails.setPaymentPage_LName(paymentDetails.getCustomerLastName());
					activityDetails.setPaymentPage_address(paymentDetails.getAddress());
					activityDetails.setPaymentPage_TP(paymentDetails.getTel());
					activityDetails.setPaymentPage_Email(paymentDetails.getEmail());
					activityDetails.setPaymentPage_country(paymentDetails.getCountry());
					activityDetails.setPaymentPage_city(paymentDetails.getCity());
					activityDetails.setPaymentPage_State(paymentDetails.getState());
					activityDetails.setPaymentPage_PostalCode(paymentDetails.getPostalCode());

					driver.findElement(By.id("cusFName")).sendKeys(paymentDetails.getCustomerName());
					driver.findElement(By.id("cusLName")).sendKeys(paymentDetails.getCustomerLastName());
					driver.findElement(By.id("cusAdd1")).sendKeys(paymentDetails.getAddress());
					driver.findElement(By.id("cusCity")).sendKeys(paymentDetails.getCity());

					String onLineTp_1 = paymentDetails.getTel().split("-")[0];
					onLineTp_2 = paymentDetails.getTel().split("-")[1];

					driver.findElement(By.id("cusareacodetext")).sendKeys(onLineTp_1);
					driver.findElement(By.id("cusPhonetext")).sendKeys(onLineTp_2);

					driver.findElement(By.id("cusEmail")).sendKeys(paymentDetails.getEmail());
					driver.findElement(By.id("cusConfEmail")).sendKeys(paymentDetails.getEmail());

					new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(paymentDetails.getCountry());
					Thread.sleep(1500);
					
					cCountry = paymentDetails.getCountry();

					if (paymentDetails.getCountry().equalsIgnoreCase("USA") || paymentDetails.getCountry().equalsIgnoreCase("Canada") || paymentDetails.getCountry().equalsIgnoreCase("Australia")) {

						if (driver.findElement(By.id("cusState")).isDisplayed() == true) {

							new Select(driver.findElement(By.id("cusState"))).selectByVisibleText(paymentDetails.getState());
							Thread.sleep(1500);
							driver.findElement(By.id("cusZip")).sendKeys(paymentDetails.getPostalCode());

						} else {
							activityDetails.setPaymentPage_State("Not Available");
						}

					}

				}

			}

			// QUOTATION req

			if (searchInfo.getQuotationReq().equalsIgnoreCase("Yes")) {
				
				String shoppingCart_subTotal = driver.findElement(By.xpath(".//*[@id='subtotal']")).getText();
				activityDetails.setShoppingCart_subTotal(shoppingCart_subTotal);
				String shoppingCart_TotalTax = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
				activityDetails.setShoppingCart_TotalTax(shoppingCart_TotalTax);
				String shoppingCart_TotalValue = driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText();
				activityDetails.setShoppingCart_TotalValue(shoppingCart_TotalValue);
				String shoppingCart_AmountNow = driver.findElement(By.xpath(".//*[@id='totalamountprocessnow']")).getText();
				activityDetails.setShoppingCart_AmountNow(shoppingCart_AmountNow);
				String shoppingCart_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='amountduetoutilazation']")).getText();
				activityDetails.setShoppingCart_AmountCheckIn(shoppingCart_AmountCheckIn);

				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {

					String paymentPageAgentComm = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/aside/section/div/div/ul/li[1]/div[2]")).getText();
					activityDetails.setPaymentsPage_AgentCommission(paymentPageAgentComm);

					if (discountYesNo.equalsIgnoreCase("Yes")) {

						String discountCoupon = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/aside/section/div/div/ul/li[2]/div[4]")).getText();
						activityDetails.setPaymentsPage_DiscountValue1(discountCoupon);

					}

				} else {

					if (discountYesNo.equalsIgnoreCase("Yes")) {

						String discountCoupon = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/aside/section/div/div/ul/li[2]/div[4]")).getText();
						activityDetails.setPaymentsPage_DiscountValue1(discountCoupon);

					}
				}
				
				
				//Click Quotation Butt
				
				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[2]/div[2]/a[1]")).click();
				driver.switchTo().defaultContent();	
				Thread.sleep(8000);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("customername")));	
				
				//QuotationConfirmation
				
				String Quote_QuotationNo = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[1]/ul/li[3]/p[2]/b")).getText();
				activityDetails.setQuote_QuotationNo(Quote_QuotationNo);
				
				String Quote_Status = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[2]/div[1]/div/p[2]")).getText();
				activityDetails.setQuote_Status(Quote_Status);
								
				String Quote_QTY = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[3]/ul[2]/li[3]")).getText();
				activityDetails.setQuote_QTY(Quote_QTY);
				
				String Quote_ActivityName = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[1]/p[1]")).getText();
				activityDetails.setQuote_ActivityName(Quote_ActivityName);
				
				String Quote_FName = driver.findElement(By.xpath(".//*[@id='customerfname']")).getText().split(": ")[1];
				activityDetails.setQuote_FName(Quote_FName);
				
				String Quote_LName = driver.findElement(By.xpath(".//*[@id='customerlname']")).getText().split(": ")[1];
				activityDetails.setQuote_LName(Quote_LName);
				
				String Quote_TP = driver.findElement(By.xpath(".//*[@id='custp']")).getText().split(": ")[1];
				activityDetails.setQuote_TP(Quote_TP);
				
				String Quote_Email = driver.findElement(By.xpath(".//*[@id='customeremail']")).getText().split(": ")[1];
				activityDetails.setQuote_Email(Quote_Email);
				
				String Quote_address_1 = driver.findElement(By.xpath(".//*[@id='customeraddress1']")).getText().split(": ")[1];
				activityDetails.setQuote_address_1(Quote_address_1);
				
				/*String Quote_address_2 = driver.findElement(By.xpath(".//*[@id='cusaddress2']")).getText();
				activityDetails.setQuote_address_2(Quote_address_2);*/
				
				String Quote_city = driver.findElement(By.xpath(".//*[@id='customercity']")).getText().split(": ")[1];
				activityDetails.setQuote_city(Quote_city);
				
				String Quote_country = driver.findElement(By.xpath(".//*[@id='customercountry']")).getText().split(": ")[1];
				activityDetails.setQuote_country(Quote_country);
				
				if (cCountry.equals("USA") || cCountry.equals("Canada") || cCountry.equals("Australia")) {
					
					String Quote_state = driver.findElement(By.xpath(".//*[@id='customerstate']")).getText().split(": ")[1];
					activityDetails.setQuote_state(Quote_state);
					
					String Quote_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText().split(": ")[1];
					activityDetails.setQuote_postalCode(Quote_postalCode);
				}
				
				
				
				String Quote_subTotal = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[5]/ul/li[2]/div[2]")).getText();
				activityDetails.setQuote_subTotal(Quote_subTotal);
				
				String Quote_taxandOther = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[5]/ul/li[3]/div[2]")).getText();
				activityDetails.setQuote_taxandOther(Quote_taxandOther);
				
				String Quote_totalValue = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[5]/ul/li[4]/div[2]")).getText();
				activityDetails.setQuote_totalValue(Quote_totalValue);
				
				String Quote_TotalGrossPackageValue = driver.findElement(By.xpath(".//*[@id='totalbeforetax']")).getText();
				activityDetails.setQuote_TotalGrossPackageValue(Quote_TotalGrossPackageValue);
				
				String Quote_TotalTax = driver.findElement(By.xpath(".//*[@id='tottaxandothercharges']")).getText();
				activityDetails.setQuote_TotalTax(Quote_TotalTax);
				
				String Quote_TotalPackageValue = driver.findElement(By.xpath(".//*[@id='totalpayable']")).getText();
				activityDetails.setQuote_TotalPackageValue(Quote_TotalPackageValue);
				
				
				
				
				
			} else {

				// No Quotation

				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[2]/div[2]/a[2]")).click();
				Thread.sleep(1000);

				// Occupancy Details

				Random rand = new Random();
				int randNumber = rand.nextInt(1000) + 5;
				ArrayList<String> cusTitle = new ArrayList<String>();
				ArrayList<String> cusFName = new ArrayList<String>();
				ArrayList<String> cusLname = new ArrayList<String>();

				ArrayList<WebElement> adultFirstlist = new ArrayList<WebElement>(driver.findElements(By.className("act-occ-first-name")));
				ArrayList<WebElement> adultLastlist = new ArrayList<WebElement>(driver.findElements(By.className("act-occ-last-name")));

				HashMap<Integer, String> map = new HashMap<Integer, String>();
				map.put(1, "one");
				map.put(2, "two");
				map.put(3, "three");
				map.put(4, "four");
				map.put(5, "five");
				map.put(6, "six");
				map.put(7, "seven");
				map.put(8, "Eight");
				map.put(9, "nine");

				Iterator<WebElement> adultIterator = adultFirstlist.iterator();
				int AdultCount = 1;

				while (adultIterator.hasNext()) {
					adultIterator.next().sendKeys("AdFirst_" + map.get(AdultCount));
					adultLastlist.get(AdultCount - 1).sendKeys("AdLast_" + map.get(AdultCount));
					cusTitle.add("Mr");
					if (AdultCount == 1) {
						cusFName.add("AdFirst_" + map.get(AdultCount));
						cusLname.add("AdLast_" + map.get(AdultCount));
					} else {
						cusFName.add("AdFirst_" + map.get(AdultCount));
						cusLname.add("AdLast_" + map.get(AdultCount));
					}

					AdultCount++;
				}

				activityDetails.setResultsPage_cusTitle(cusTitle);
				activityDetails.setResultsPage_cusFName(cusFName);
				activityDetails.setResultsPage_cusLName(cusLname);

				// Pickup / DropOff Details

				String programId;

				try {

					WebElement pickUplement = driver.findElement(By.name("programId"));
					programId = pickUplement.getAttribute("value");

					WebElement pickDetailsContainerElement = driver.findElement(By.xpath(".//*[@id='pickupdropoffdetailsnotadd" + programId + "']/div[2]/div"));
					String bidIdForPickUp = pickDetailsContainerElement.findElement(By.tagName("p")).getAttribute("id").split("Name-")[1];

					// DropOff details
					driver.findElement(By.name("pickuplocationStationPortNameTxt-" + bidIdForPickUp + "")).sendKeys("Colombo");
					driver.findElement(By.name("pickupaddionalinfo-" + bidIdForPickUp + "")).sendKeys("PickUp Additional Info");
					driver.findElement(By.name("dropofflocationNameStationPortTxt-" + bidIdForPickUp + "")).sendKeys("Hambanthota");
					driver.findElement(By.name("dropoffaddionalinfo-" + bidIdForPickUp + "")).sendKeys("DropOff Additional Info");

				} catch (Exception e) {
					e.printStackTrace();
				}

				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[4]/div[2]/a[2]")).click();
				Thread.sleep(1000);

				// Billing Info

				// Discount coupon

				if (discountYesNo.equalsIgnoreCase("Yes")) {

					driver.findElement(By.xpath(".//*[@id='couponno']")).sendKeys(PG_Properties.getProperty("DiscountCouponNumber"));
					driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[2]/a")).click();
					Thread.sleep(2000);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[6]/div[3]/div/button")));
					driver.findElement(By.xpath("html/body/div[6]/div[3]/div/button")).click();

					// Filling again

					driver.findElement(By.id("cusPhonetext")).clear();
					driver.findElement(By.id("cusPhonetext")).sendKeys(onLineTp_2);

					driver.findElement(By.id("cusmobnos")).clear();
					driver.findElement(By.id("cusmobnoe")).clear();

					driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[2]/div[2]/a[2]")).click();
					Thread.sleep(1000);

					driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[4]/div[2]/a[2]")).click();
					Thread.sleep(1000);

				}

								
				//BillingInfo
				
				/*new Select(driver.findElement(By.id("credit_card_type"))).selectByValue("mastercard");
		        Thread.sleep(1000);*/
				
				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO ) {
					
					if (paymentType.equalsIgnoreCase("Pay Offline")) {
						
						driver.findElement(By.xpath(".//*[@id='pay_offline']")).click();
						Thread.sleep(2000);
						
						new Select(driver.findElement(By.xpath(".//*[@id='payment_method_1']"))).selectByVisibleText("Cash");
						activityDetails.setPaymentMethod_PaymentsPage("Cash");
						driver.findElement(By.xpath(".//*[@id='offlinePaymentMethodsRow']/div[2]/input")).sendKeys("123456");
						activityDetails.setPaymentReference_PaymentsPage("123456");
						
						Thread.sleep(1000);
						
						
						try {
							driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[3]/a[2]")).click();
						} catch (Exception e1) {
							driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[4]/a[2]")).click();
						}
						
						Thread.sleep(2000);
												
					}else{
						
						String paymentBilling_CardTotal = driver.findElement(By.xpath(".//*[@id='totchgpgamt']")).getText();
						activityDetails.setPaymentBilling_CardTotal(paymentBilling_CardTotal);
						
						try {
							String paymentBilling_CardCurrency = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[6]/div[3]/div/div[2]/ul/li[1]/div")).getText();
							activityDetails.setPaymentBilling_CardCurrency(paymentBilling_CardCurrency);
						} catch (Exception e) {
							String paymentBilling_CardCurrency = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[3]/div/div[4]/ul/li[1]/div")).getText();
							activityDetails.setPaymentBilling_CardCurrency(paymentBilling_CardCurrency);
						}
						
						
						String paymentBilling_subTotal = driver.findElement(By.xpath(".//*[@id='paymentdetails_subtotal']")).getText();
						activityDetails.setPaymentBilling_subTotal(paymentBilling_subTotal);
						String paymentBilling_TotalTax = driver.findElement(By.xpath(".//*[@id='totaltaxpgcurrid']")).getText();
						activityDetails.setPaymentBilling_TotalTax(paymentBilling_TotalTax);
						String paymentBilling_TotalValue = driver.findElement(By.xpath(".//*[@id='totalchargeamtpgcurrid']")).getText();
						activityDetails.setPaymentBilling_TotalValue(paymentBilling_TotalValue);
						String paymentBilling_AmountNow = driver.findElement(By.xpath(".//*[@id='totalpaynowamountpgcurrid']")).getText();
						activityDetails.setPaymentBilling_AmountNow(paymentBilling_AmountNow);
						String paymentBilling_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='totalamountatutilizationpgcurrid']")).getText();
						activityDetails.setPaymentBilling_AmountCheckIn(paymentBilling_AmountCheckIn);
						
						
						if (discountYesNo.equalsIgnoreCase("Yes")) {

							String discountCoupon = driver.findElement(By.xpath(".//*[@id='paymentdetails_discount']")).getText();
							activityDetails.setPaymentsPage_DiscountValue2(discountCoupon);

						}
						
						Thread.sleep(1000);
						try {
							driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[3]/a[2]")).click();
						} catch (Exception e1) {
							driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[4]/a[2]")).click();
						}
						
						Thread.sleep(2000);
						
						
					}
						
				}else if(UserTypeDC_B2B == TourOperatorType.NETCASH || UserTypeDC_B2B == TourOperatorType.NETCREDITLPOY || UserTypeDC_B2B == TourOperatorType.NETCREDITLPON) {
				
					if (paymentType.equalsIgnoreCase("Pay Offline")) {
						
						driver.findElement(By.xpath(".//*[@id='pay_offline']")).click();
						Thread.sleep(2000);
						
						Thread.sleep(1000);
						try {
							driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[3]/a[2]")).click();
						} catch (Exception e1) {
							driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[4]/a[2]")).click();
						}
						
						Thread.sleep(2000);
												
					}else{
						
						String paymentBilling_CardTotal = driver.findElement(By.xpath(".//*[@id='totchgpgamt']")).getText();
						activityDetails.setPaymentBilling_CardTotal(paymentBilling_CardTotal);
						String paymentBilling_CardCurrency = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[3]/div/div[4]/ul/li[1]/div")).getText();
						activityDetails.setPaymentBilling_CardCurrency(paymentBilling_CardCurrency);
						
						String paymentBilling_subTotal = driver.findElement(By.xpath(".//*[@id='paymentdetails_subtotal']")).getText();
						activityDetails.setPaymentBilling_subTotal(paymentBilling_subTotal);
						String paymentBilling_TotalTax = driver.findElement(By.xpath(".//*[@id='totaltaxpgcurrid']")).getText();
						activityDetails.setPaymentBilling_TotalTax(paymentBilling_TotalTax);
						String paymentBilling_TotalValue = driver.findElement(By.xpath(".//*[@id='totalchargeamtpgcurrid']")).getText();
						activityDetails.setPaymentBilling_TotalValue(paymentBilling_TotalValue);
						String paymentBilling_AmountNow = driver.findElement(By.xpath(".//*[@id='totalpaynowamountpgcurrid']")).getText();
						activityDetails.setPaymentBilling_AmountNow(paymentBilling_AmountNow);
						String paymentBilling_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='totalamountatutilizationpgcurrid']")).getText();
						activityDetails.setPaymentBilling_AmountCheckIn(paymentBilling_AmountCheckIn);
						
						if (discountYesNo.equalsIgnoreCase("Yes")) {

							String discountCoupon = driver.findElement(By.xpath(".//*[@id='paymentdetails_discount']")).getText();
							activityDetails.setPaymentsPage_DiscountValue2(discountCoupon);

						}
						
						Thread.sleep(1000);
						try {
							driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[3]/a[2]")).click();
						} catch (Exception e1) {
							driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[4]/a[2]")).click();
						}
						
						Thread.sleep(2000);
					}
				
				}else{
					
					String paymentBilling_CardTotal = driver.findElement(By.xpath(".//*[@id='totchgpgamt']")).getText();
					activityDetails.setPaymentBilling_CardTotal(paymentBilling_CardTotal);
					String paymentBilling_CardCurrency = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[6]/div[3]/div/div[2]/ul/li[1]/div")).getText();
					activityDetails.setPaymentBilling_CardCurrency(paymentBilling_CardCurrency);
					
					String paymentBilling_subTotal = driver.findElement(By.xpath(".//*[@id='paymentdetails_subtotal']")).getText();
					activityDetails.setPaymentBilling_subTotal(paymentBilling_subTotal);
					String paymentBilling_TotalTax = driver.findElement(By.xpath(".//*[@id='totaltaxpgcurrid']")).getText();
					activityDetails.setPaymentBilling_TotalTax(paymentBilling_TotalTax);
					String paymentBilling_TotalValue = driver.findElement(By.xpath(".//*[@id='totalchargeamtpgcurrid']")).getText();
					activityDetails.setPaymentBilling_TotalValue(paymentBilling_TotalValue);
					String paymentBilling_AmountNow = driver.findElement(By.xpath(".//*[@id='totalpaynowamountpgcurrid']")).getText();
					activityDetails.setPaymentBilling_AmountNow(paymentBilling_AmountNow);
					String paymentBilling_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='totalamountatutilizationpgcurrid']")).getText();
					activityDetails.setPaymentBilling_AmountCheckIn(paymentBilling_AmountCheckIn);
					
					Thread.sleep(1000);
					try {
						driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[3]/a[2]")).click();
					} catch (Exception e1) {
						driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[6]/div[4]/a[2]")).click();
					}
					
					Thread.sleep(2000);
					
				}
				
				
				
				// My basket

				String shoppingCart_subTotal = driver.findElement(By.xpath(".//*[@id='subtotal']")).getText();
				activityDetails.setShoppingCart_subTotal(shoppingCart_subTotal);
				String shoppingCart_TotalTax = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
				activityDetails.setShoppingCart_TotalTax(shoppingCart_TotalTax);
				String shoppingCart_TotalValue = driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText();
				activityDetails.setShoppingCart_TotalValue(shoppingCart_TotalValue);
				String shoppingCart_AmountNow = driver.findElement(By.xpath(".//*[@id='totalamountprocessnow']")).getText();
				activityDetails.setShoppingCart_AmountNow(shoppingCart_AmountNow);
				String shoppingCart_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='amountduetoutilazation']")).getText();
				activityDetails.setShoppingCart_AmountCheckIn(shoppingCart_AmountCheckIn);

				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {

					String paymentPageAgentComm = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/aside/section/div/div/ul/li[1]/div[2]")).getText();
					activityDetails.setPaymentsPage_AgentCommission(paymentPageAgentComm);

					if (discountYesNo.equalsIgnoreCase("Yes")) {

						String discountCoupon = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/aside/section/div/div/ul/li[2]/div[4]")).getText();
						activityDetails.setPaymentsPage_DiscountValue1(discountCoupon);

					}

				} else {

					if (discountYesNo.equalsIgnoreCase("Yes")) {

						String discountCoupon = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/aside/section/div/div/ul/li[2]/div[4]")).getText();
						activityDetails.setPaymentsPage_DiscountValue1(discountCoupon);

					}
				}
				
				
				
				// Notes

				String cusNotes = "I am wanting to generate a random string of 20 characters without " + "using apache classes. I don't really care about whether is "
						+ "alphanumeric or not. Also, I am going to convert it to an array of bytes later FYI";
				driver.findElement(By.id("txt_cus_notes")).sendKeys(cusNotes);
				activityDetails.setCustomerNotes(cusNotes);

				String actNotes = "Activity notes displaying here.";
				driver.findElement(By.xpath(".//*[@id='activitynotesid0']/textarea")).sendKeys(actNotes);
				activityDetails.setActivityNotes(actNotes);
				Thread.sleep(1000);

				driver.findElement(By.xpath(".//*[@id='paynotes']/div[3]/a[2]")).click();
				Thread.sleep(1000);

				// Terms and condition

				WebElement cancelElementlistPayment = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_" + idOnly + "']/div[3]"));
				List<WebElement> liElementPay = cancelElementlistPayment.findElements(By.tagName("li"));

				ArrayList<String> cancelListPay = new ArrayList<String>();

				for (int i = 1; i <= liElementPay.size(); i++) {

					String cancelPolicies = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_" + idOnly + "']/div[3]/ul/li[" + i + "]")).getText();
					cancelListPay.add(cancelPolicies);

				}

				activityDetails.setPaymentCancelPolicy(cancelListPay);

				driver.findElement(By.xpath(".//*[@id='confreadPolicy']")).click();
				driver.findElement(By.xpath(".//*[@id='confTnC']")).click();

				driver.findElement(By.xpath(".//*[@id='confirm-payment-btn']")).click();
				Thread.sleep(8500);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='divConfirm']/a")));

				// Popup window

				String paymentPopUp_ActivityName = driver.findElement(By.xpath(".//*[@id='header']/div[6]/div/div[2]/div/div[1]/ul[2]/li[1]/div")).getText();
				activityDetails.setPaymentPopUp_ActivityName(paymentPopUp_ActivityName);

				driver.findElement(By.xpath(".//*[@id='divConfirm']/a")).click();
				availablClick = (System.currentTimeMillis() / 1000);
				activityDetails.setTimeAailableClick(availablClick);
				Thread.sleep(30000);
				
				if (paymentType.equalsIgnoreCase("Pay Online")) {
					
					//Payment gateway
					
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paygatewayFrame")));
					driver.switchTo().frame("paygatewayFrame");
					
					File scrFile256 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			        FileUtils.copyFile(scrFile256, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_PaymentGateway.png"));
			       					
					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("document.getElementById('cardnumberpart1').value = '4000';");
					jse.executeScript("document.getElementById('cardnumberpart2').value = '0000';");
					jse.executeScript("document.getElementById('cardnumberpart3').value = '0000';");
					jse.executeScript("document.getElementById('cardnumberpart4').value = '0002';");
				
					new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText("01");
					new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText("2016");
					
					Thread.sleep(1000);
					driver.findElement(By.xpath(".//*[@id='cardholdername']")).sendKeys("aaa");
					jse.executeScript("document.getElementById('cv2').value = '111';");
			        
			        /*JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("document.getElementById('cardnumberpart1').value = '5500';");
					jse.executeScript("document.getElementById('cardnumberpart2').value = '0055';");
					jse.executeScript("document.getElementById('cardnumberpart3').value = '5555';");
					jse.executeScript("document.getElementById('cardnumberpart4').value = '5559';");
				
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText("12");
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText("2018");
					
					driver.findElement(By.xpath(".//*[@id='cardholdername']")).sendKeys("Asiri");
					jse.executeScript("document.getElementById('cv2').value = '111';");*/
					
					String amount = driver.findElement(By.xpath(".//*[@id='main_dev']/div[6]/div[2]")).getText();
					activityDetails.setPaymentGatewayTotal(amount);
								
					driver.findElement(By.xpath(".//*[@id='main_dev']/div[7]/div[2]/a/div")).click();	
					Thread.sleep(20000);
					
				}
				
				
				// Confirmation Page

				driver.switchTo().defaultContent();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.className("confirmation-block-wrapper")));

				File scrFile_29 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile_29, new File("" + currentDateforImages + "/" + searchInfo.getScenarioCount() + "_ConfirmationPage.png"));

				String referenceName = driver.findElement(By.xpath(".//*[@id='customername']")).getText().split(": ")[1];
				activityDetails.setConfirmation_BookingRefference(referenceName);
				String bookingNo = driver.findElement(By.xpath(".//*[@id='activityresno']")).getText();
				comfirmBooking = (System.currentTimeMillis() / 1000);
				activityDetails.setTimeConfirmBooking(comfirmBooking);
				activityDetails.setReservationNo(bookingNo);
				String confirmation_CusMailAddress = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[1]/ul/li[4]/p/span")).getText();
				activityDetails.setConfirmation_CusMailAddress(confirmation_CusMailAddress);

				String confirmation_BI_ActivityName = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[1]/p[1]")).getText();
				activityDetails.setConfirmation_BI_ActivityName(confirmation_BI_ActivityName);

				String confirmation_BI_City = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[1]/p[2]")).getText();
				activityDetails.setConfirmation_BI_City(confirmation_BI_City);

				String confirmation_BI_ActType = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[1]/p[3]")).getText();
				activityDetails.setConfirmation_BI_ActType(confirmation_BI_ActType);

				String confirmation_BI_BookingStatus = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[2]/div[1]/div/p[2]")).getText();
				activityDetails.setConfirmation_BI_BookingStatus(confirmation_BI_BookingStatus);

				String confirmation_BI_SelectedDate = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[2]/div[2]/div/p[2]")).getText();
				activityDetails.setConfirmation_BI_SelectedDate(confirmation_BI_SelectedDate);
				// /

				ArrayList<String> confirmationPage_RateType = new ArrayList<String>();
				ArrayList<String> confirmationPage_DailyRate = new ArrayList<String>();
				ArrayList<String> confirmationPage_QTY = new ArrayList<String>();

				List<WebElement> confirmationActlist = driver.findElements(By.className("activity-table-body"));

				for (int i = 1; i <= confirmationActlist.size(); i++) {

					String confirmationRates = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[3]/ul[" + (i + 1) + "]/li[1]")).getText();
					confirmationPage_RateType.add(confirmationRates);

					String confirmationCost = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[3]/ul[" + (i + 1) + "]/li[2]")).getText();
					confirmationPage_DailyRate.add(confirmationCost);

					String confirmationQTY = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[3]/ul[" + (i + 1) + "]/li[3]")).getText();
					confirmationPage_QTY.add(confirmationQTY);

				}

				activityDetails.setConfirmation_RateType(confirmationPage_RateType);
				activityDetails.setConfirmation_DailyRate(confirmationPage_DailyRate);
				activityDetails.setConfirmation_QTY(confirmationPage_QTY);
				
				try {
					activityDetails.setPaymentReference_bookingList(driver.findElement(By.xpath(".//*[@id='merchanttrackid']")).getText());
					activityDetails.setAuthentReference_bookingList(driver.findElement(By.xpath(".//*[@id='merchanttrackid']")).getText());
					activityDetails.setPaymentID(driver.findElement(By.xpath(".//*[@id='merchanttrackid']")).getText());
					activityDetails.setPaymentType_bookingList("Credit Card");
				} catch (Exception e1) {
					activityDetails.setPaymentReference_bookingList("N/A");
					activityDetails.setAuthentReference_bookingList("N/A");
					activityDetails.setPaymentID("N/A");
					
					if (UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
						activityDetails.setPaymentType_bookingList("On Credit");						
					}else{
						activityDetails.setPaymentType_bookingList("Offline");
					}
								
				}
				
				

				// //

				String confirmation_Currency1 = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[4]/ul/li[1]/div")).getText();
				activityDetails.setConfirmation_Currency1(confirmation_Currency1);
				String confirmation_Currency2 = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/div[1]/ul/li[1]/div")).getText();
				activityDetails.setConfirmation_Currency2(confirmation_Currency2);

				String confirmation_SubTotal = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[5]/ul/li[2]/div[2]")).getText();
				activityDetails.setConfirmation_SubTotal(confirmation_SubTotal);

				String confirmation_Tax = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[5]/ul/li[3]/div[2]")).getText();
				activityDetails.setConfirmation_Tax(confirmation_Tax);

				String confirmation_Total = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[5]/ul/li[4]/div[2]")).getText();
				activityDetails.setConfirmation_Total(confirmation_Total);

				if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {

					String setConfirmationPage_AgentCommission = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/div[1]/ul/li[2]/div[2]")).getText();
					activityDetails.setConfirmationPage_AgentCommission(setConfirmationPage_AgentCommission);

					if (discountYesNo.equalsIgnoreCase("Yes")) {

						String discountCoupon = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/div[1]/ul/li[4]/div[2]")).getText();
						activityDetails.setConfirmationPage_DiscountValue1(discountCoupon);

					}

				} else {
					if (discountYesNo.equalsIgnoreCase("Yes")) {

						String discountCoupon = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/div[1]/ul/li[3]/div[2]")).getText();
						activityDetails.setConfirmationPage_DiscountValue1(discountCoupon);

					}
				}

				String confirmation_SubTotal2 = driver.findElement(By.xpath(".//*[@id='totalbeforetax']")).getText();
				activityDetails.setConfirmation_SubTotal2(confirmation_SubTotal2);

				String confirmation_TotalTaxOtherCharges = driver.findElement(By.xpath(".//*[@id='tottaxandothercharges']")).getText();
				activityDetails.setConfirmation_TotalTaxOtherCharges(confirmation_TotalTaxOtherCharges);

				String confirmation_Total2 = driver.findElement(By.xpath(".//*[@id='totalpayable']")).getText();
				activityDetails.setConfirmation_Total2(confirmation_Total2);

				String confirmation_AmountNow = driver.findElement(By.xpath(".//*[@id='amountbeingprocessednow']")).getText();
				activityDetails.setConfirmation_AmountNow(confirmation_AmountNow);

				String confirmation_AmountDueCheckIn = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
				activityDetails.setConfirmation_AmountDueCheckIn(confirmation_AmountDueCheckIn);

				// ////

				String confirmation_FName = driver.findElement(By.xpath(".//*[@id='customerfname']")).getText().split(": ")[1];
				activityDetails.setConfirmation_FName(confirmation_FName);

				String confirmation_LName = driver.findElement(By.xpath(".//*[@id='customerlname']")).getText().split(": ")[1];
				activityDetails.setConfirmation_LName(confirmation_LName);

				String confirmation_TP;
				try {
					confirmation_TP = driver.findElement(By.xpath(".//*[@id='cusemergencycontact']")).getText().split(": ")[1];
					activityDetails.setConfirmation_TP(confirmation_TP);
				} catch (Exception e) {
					confirmation_TP = "Not Available";
					activityDetails.setConfirmation_TP(confirmation_TP);
				}

				String confirmation_Email = driver.findElement(By.xpath(".//*[@id='customeremail']")).getText().split(": ")[1];
				activityDetails.setConfirmation_Email(confirmation_Email);

				String confirmation_address = driver.findElement(By.xpath(".//*[@id='customeraddress1']")).getText().split(": ")[1];
				activityDetails.setConfirmation_address(confirmation_address);

				String confirmation_country = driver.findElement(By.xpath(".//*[@id='customercountry']")).getText().split(": ")[1];
				activityDetails.setConfirmation_country(confirmation_country);

				String confirmation_city = driver.findElement(By.xpath(".//*[@id='customercity']")).getText().split(": ")[1];
				activityDetails.setConfirmation_city(confirmation_city);

				String confirmation_State = driver.findElement(By.xpath(".//*[@id='customerstate']")).getText().split(": ")[1];
				activityDetails.setConfirmation_State(confirmation_State);

				String confirmation_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText().split(": ")[1];
				activityDetails.setConfirmation_postalCode(confirmation_postalCode);

				// //confirmation_postalCode

				String confirmation_AODActivityName = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[3]/h3")).getText().split(" - ")[1];
				activityDetails.setConfirmation_AODActivityName(confirmation_AODActivityName);

				try {
					String confirmation_BillingInfo_TotalValue = driver.findElement(By.xpath(".//*[@id='paymentamount']")).getText();
					activityDetails.setConfirmation_BillingInfo_TotalValue(confirmation_BillingInfo_TotalValue);
				} catch (Exception e) {
					String confirmation_BillingInfo_TotalValue = "N/A";
					activityDetails.setConfirmation_BillingInfo_TotalValue(confirmation_BillingInfo_TotalValue);
				}

				ArrayList<String> confirmTitle = new ArrayList<String>();
				ArrayList<String> confirmFirstName = new ArrayList<String>();
				ArrayList<String> confirmLastName = new ArrayList<String>();

				List<WebElement> confirmationPaxlist = driver.findElements(By.className("car-details-body"));

				for (int i = 1; i <= confirmationPaxlist.size(); i++) {

					String confirmationT = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[3]/div[3]/div/ul[" + (i + 1) + "]/li[2]")).getText();
					confirmTitle.add(confirmationT);

					String confirmationFN = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[3]/div[3]/div/ul[" + (i + 1) + "]/li[3]")).getText();
					confirmFirstName.add(confirmationFN);

					String confirmationLN = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[3]/div[3]/div/ul[" + (i + 1) + "]/li[4]")).getText();
					confirmLastName.add(confirmationLN);

				}

				activityDetails.setConfirmationPage_cusTitle(confirmTitle);
				activityDetails.setConfirmationPage_cusFName(confirmFirstName);
				activityDetails.setConfirmationPage_cusLName(confirmLastName);

				// /

				WebElement cancelElementlist = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_" + idOnly + "']/div[3]"));
				List<WebElement> liElement = cancelElementlist.findElements(By.tagName("li"));

				ArrayList<String> cancelList = new ArrayList<String>();

				for (int i = 1; i <= liElement.size(); i++) {

					String cancelPolicies = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_" + idOnly + "']/div[3]/ul/li[" + i + "]")).getText();
					cancelList.add(cancelPolicies);

				}

				activityDetails.setConfirmCancelPolicy(cancelList);

			}

		}

		return activityDetails;
	}
	
	
	public Quotation getQuotationMail(WebDriver driver2) throws InterruptedException{
		
		Quotation quotationDetails = new Quotation();
		WebDriverWait wait = new WebDriverWait(driver, 200);	
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDate = dateFormatcurrentDate.format(cal.getTime());
		String parentHandle = null;
						
		try {
			
			driver.get(""+PG_Properties.getProperty("Quotation")+""+currentDate+"");
						
			WebElement mailTable = driver.findElement(By.xpath(".//*[@id='filetable']"));
			List<WebElement> trElement = mailTable.findElements(By.tagName("tr"));
			
			for (int i = 4; i < trElement.size(); i++) {
				
				String quotationNumber = driver.findElement(By.xpath(".//*[@id='filetable']/tbody/tr["+i+"]/td[2]/a")).getText();
				//activitydetails.getQuote_QuotationNo()
				if (quotationNumber.contains(activityDetails.getQuote_QuotationNo())) {
					
					driver.findElement(By.xpath(".//*[@id='filetable']/tbody/tr["+i+"]/td[2]/a")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='filetable']/tbody/tr[4]/td[2]/a")).click();					
					Thread.sleep(2000);
					
					parentHandle = driver.getWindowHandle();
					for (String winHandle : driver.getWindowHandles()) {
				        driver.switchTo().window(winHandle); 
				    }
					
				    Thread.sleep(3000);
				    
				    File cCEQ = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			        FileUtils.copyFile(cCEQ, new File(""+currentDateforImages+"/"+activityDetails.getScenarioCount()+"_QuotationMail.png"));
					
					String companyTel = driver.findElement(By.xpath("html/body/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
					quotationDetails.setCompanyTel(companyTel);
					
					String companyFax = driver.findElement(By.xpath("html/body/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
					quotationDetails.setCompanyFax(companyFax);
					
					String companyEmail = driver.findElement(By.xpath("html/body/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
					quotationDetails.setCompanyEmail(companyEmail);
					
					String companyWeb = driver.findElement(By.xpath("html/body/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
					quotationDetails.setCompanyWeb(companyWeb);
					
					String companyName = driver.findElement(By.xpath("html/body/table/tbody/tr[36]/td/div/strong")).getText();
					quotationDetails.setCompanyName(companyName);
					
					
					String refNo = driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/div/strong")).getText().split("\\.")[1].replaceAll(" ", "");
					quotationDetails.setRefNo(refNo);
					
					String referenceName = driver.findElement(By.xpath("html/body/table/tbody/tr[4]/td/strong")).getText().replaceAll(" ", "");
					quotationDetails.setReferenceName(referenceName);
					
					String activityName = driver.findElement(By.xpath("html/body/table/tbody/tr[13]/td[2]")).getText().split(": ")[1];
					quotationDetails.setActivityName(activityName);
					
					String activityDate = driver.findElement(By.xpath("html/body/table/tbody/tr[14]/td[2]")).getText().split(": ")[1];
					quotationDetails.setActivityDate(activityDate);
					
					String periodType = driver.findElement(By.xpath("html/body/table/tbody/tr[15]/td[2]")).getText().split(": ")[1];
					quotationDetails.setPeriodType(periodType);
					
					String rateType = driver.findElement(By.xpath("html/body/table/tbody/tr[16]/td[2]")).getText().split(": ")[1];
					quotationDetails.setRateType(rateType);
					
					String bookingStatus = driver.findElement(By.xpath("html/body/table/tbody/tr[17]/td[2]")).getText().split(": ")[1];
					quotationDetails.setBookingStatus(bookingStatus);
					
					String personRate = driver.findElement(By.xpath("html/body/table/tbody/tr[18]/td[2]")).getText().split(": ")[1].split("\\.")[0];
					quotationDetails.setPersonRate(personRate);
					
					String qty = driver.findElement(By.xpath("html/body/table/tbody/tr[19]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQty(qty);
					
					String currency = driver.findElement(By.xpath("html/body/table/tbody/tr[20]/td[1]/strong")).getText().split(" ")[1];
					quotationDetails.setCurrency(currency);
					
					String totalRate = driver.findElement(By.xpath("html/body/table/tbody/tr[20]/td[2]/strong")).getText().split(": ")[1].split("\\.")[0];
					quotationDetails.setTotalRate(totalRate);
					
					
					
					String currency2 = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[1]/td[2]")).getText();
					quotationDetails.setCurrency2(currency2);
					
					String subTotal = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[2]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setSubTotal(subTotal);
					String tax = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[3]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setTax(tax);
					String totalValue = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[4]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setTotalValue(totalValue);
					String amountPayNow = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[5]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setAmountPayNow(amountPayNow);
					String payAtCheckIn = driver.findElement(By.xpath("html/body/table/tbody/tr[22]/td[7]/table/tbody/tr[6]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setPayAtCheckIn(payAtCheckIn);
					
					
					String subTotal2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[2]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setSubTotal2(subTotal2);
					String tax2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[3]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setTax2(tax2);
					String totalBookingValue2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[5]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setTotalBookingValue2(totalBookingValue2);
					String amountPayNow2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[6]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setAmountPayNow2(amountPayNow2);
					String payAtCheckIn2 = driver.findElement(By.xpath("html/body/table/tbody/tr[24]/td[7]/table/tbody/tr[7]/td[2]")).getText().split("\\.")[0];
					quotationDetails.setPayAtCheckIn2(payAtCheckIn2);
					
					
					String cus_FName = driver.findElement(By.xpath("html/body/table/tbody/tr[27]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCus_FName(cus_FName);
					String cus_LName = driver.findElement(By.xpath("html/body/table/tbody/tr[27]/td[8]")).getText().split(": ")[1];
					quotationDetails.setCus_LName(cus_LName);
					String cusTP = driver.findElement(By.xpath("html/body/table/tbody/tr[29]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCusTP(cusTP);
					String cusEmerGencyCon = driver.findElement(By.xpath("html/body/table/tbody/tr[29]/td[8]")).getText().split(": ")[1];
					quotationDetails.setCusEmerGencyCon(cusEmerGencyCon);
					String cusEmail = driver.findElement(By.xpath("html/body/table/tbody/tr[32]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCusEmail(cusEmail);
					String cusAddress = driver.findElement(By.xpath("html/body/table/tbody/tr[28]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCusAddress(cusAddress);
					String cuscountry = driver.findElement(By.xpath("html/body/table/tbody/tr[30]/td[2]")).getText().split(": ")[1];
					quotationDetails.setCuscountry(cuscountry);
					String cuscity = driver.findElement(By.xpath("html/body/table/tbody/tr[31]/td[8]")).getText().split(": ")[1];
					quotationDetails.setCuscity(cuscity);				
					
					if (activityDetails.getPaymentPage_country().equalsIgnoreCase("USA") || activityDetails.getPaymentPage_country().equalsIgnoreCase("Canada") || activityDetails.getPaymentPage_country().equalsIgnoreCase("Australia")) {
						
						String cusState = driver.findElement(By.xpath("html/body/table/tbody/tr[30]/td[8]")).getText();
						quotationDetails.setCusState(cusState);
						String cusPostalCode = driver.findElement(By.xpath("html/body/table/tbody/tr[31]/td[2]")).getText();
						quotationDetails.setCusPostalCode(cusPostalCode);
						
					}	
					
					
				    break;
				}
				
				
								
			}
			
			driver.close(); // close newly opened window when done with it
		    driver.switchTo().window(parentHandle); // switch back to the original window
			
		    Thread.sleep(6000);
		    
			
			
			
		} catch (Exception e) {
			quotationDetails.setQuotationMailSent(false);
		}
		
		
		
		return quotationDetails;
	}
	
	
	

	
	
}
