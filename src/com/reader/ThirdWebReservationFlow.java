package com.reader;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.*;

public class ThirdWebReservationFlow {
	
	private WebDriver driver = null;
	private ThirdSearch searchInfo;
	private int childCountFromexcel;
	private int divCount;
	private List<String> valueList;
	private String currentDate, currentDateforImages,traceID;
	private String idOnly, IdStatus, idNo2, idForRemove, idNoFor2, selectedDateExcel, cancelID;
	private ActivityDetails activityDetails ;
	private long resultsAvailable;
	private long searchButtClick;
	private long loadPayment, addedtoCart, availablClick;
	private long comfirmBooking, firstSearch, firstResultsAvail;
	private int hbAvtivity = 0;
	private int gtaActivity = 0;
	private int rezAvtivity = 0;
	
	public ThirdWebReservationFlow(WebDriver Driver){
		
		this.driver = Driver;
	}
	
	public void addSearchScenario(ThirdSearch searchInfoDe){
		
		this.searchInfo = searchInfoDe;
	}
	
	public WebDriver searchEngine(WebDriver webdriver) throws InterruptedException, ParseException, IOException{
		
		activityDetails = new ActivityDetails();
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		valueList = new ArrayList<String>();
	
		driver.get(PG_Properties.getProperty("Activity_Temp5"));
		Thread.sleep(1500);
			
		driver.switchTo().defaultContent();
		driver.switchTo().frame("bec_container_frame");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hLi")));
		 		
		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
		Thread.sleep(1500);
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_BookingEngine.png"));
       	
		if (driver.findElements(By.id("AC_Country")).size() == 0) {
			activityDetails.setBookingEngineLoaded(false);
		}
		
		/////
		
		if (driver.findElements(By.id("AC_Country")).size() == 0) {
			activityDetails.setCountryLoaded(false);
		}
		if (driver.findElements(By.id("activity_Loc")).size() == 0) {
			activityDetails.setCityLoaded(false);
		}
		if (driver.findElements(By.id("ac_departure_temp")).size() == 0) {
			activityDetails.setDateFromLoaded(false);
		}
		if (driver.findElements(By.id("ac_arrival_temp")).size() == 0) {
			activityDetails.setDateToLoaded(false);
		}
		if (driver.findElements(By.id("R1occAdults_A")).size() == 0) {
			activityDetails.setAdultsLoaded(false);
		}
		if (driver.findElements(By.id("R1occChildren_A")).size() == 0) {
			activityDetails.setChildLoaded(false);
		}
		
		
		if (!(driver.findElement(By.xpath(".//*[@id='AC_Country']/option[3]")).getText().equalsIgnoreCase("Albania"))) {
			activityDetails.setCountryListLoaded(false);
		}
		
		
		driver.findElement(By.id("ac_departure_temp")).click();
		File scrFile22 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile22, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_BookingEngine_CalenderAvailable.png"));
		if (driver.findElement(By.id("ui-datepicker-div")).isDisplayed() == false) {
			activityDetails.setCalenderAvailble(false);
		}
		
		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
		Thread.sleep(1500);
		
		String showAdd = driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).getText();
		activityDetails.setShowAdd(showAdd);
		
		driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).click();
		Thread.sleep(500);
		
		File scrFile23 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile23, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_BookingEngine_AdditionalSearchOptions.png"));
        Thread.sleep(500);
		
		if (driver.findElements(By.id("activity_type")).size() == 0) {
			activityDetails.setActivityType(false);
		}
		if (driver.findElements(By.id("activityTypeId_a")).size() == 0) {
			activityDetails.setProgramCat(false);
		}
		if (driver.findElements(By.id("AC_consumerCurrencyCode")).size() == 0) {
			activityDetails.setPreCurrency(false);
		}
		if (driver.findElements(By.id("discountCoupon_No_A")).size() == 0) {
			activityDetails.setPromoCode(false);
		}
		
		if (!(driver.findElement(By.xpath(".//*[@id='AC_consumerCurrencyCode']/option[2]")).getText().equalsIgnoreCase("AED"))) {
			activityDetails.setCurrencyListLoaded(false);
		}
		
		if (!(driver.findElement(By.xpath(".//*[@id='activityTypeId_a']/option[2]")).getText().equalsIgnoreCase("Transfer"))) {
			activityDetails.setProgCatListLoaded(false);
		}
		
		
		
		String hideAdd = driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).getText();
		activityDetails.setHideAdd(hideAdd);
		
		driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).click();
		Thread.sleep(500);
		
		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
		Thread.sleep(1500);
		
		////
		
		new Select(driver.findElement(By.id("AC_Country"))).selectByVisibleText(searchInfo.getCountry());
		
		String hiddenDest = searchInfo.getDestination();		
		driver.findElement(By.id("activity_Loc")).sendKeys(hiddenDest);
		Thread.sleep(1000);
		driver.findElement(By.partialLinkText("United Kingdom")).click();
			
		
		((JavascriptExecutor)driver).executeScript("$('#ac_departure').val('"+searchInfo.getDateFrom()+"');");
     	((JavascriptExecutor)driver).executeScript("$('#ac_arrival').val('"+searchInfo.getDateTo()+"');");
		
		new Select(driver.findElement(By.id("R1occAdults_A"))).selectByVisibleText(searchInfo.getAdults());
		new Select(driver.findElement(By.id("R1occChildren_A"))).selectByVisibleText(searchInfo.getChildren());
		
		childCountFromexcel = Integer.parseInt(searchInfo.getChildren());
		
		if (searchInfo.getAgeOfChildren().contains("#")) {
			
			String ages = searchInfo.getAgeOfChildren();
			String[] partsEA = ages.split("#");
			for (String value : partsEA) {
				valueList.add(value);
			}
		}
		
		else{
			String ages = searchInfo.getAgeOfChildren();
			valueList.add(ages);
		}
		
		
		for (int i = 0; i < childCountFromexcel; i++) {
			
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("A_R1childage_"+(i+1)+""))).selectByVisibleText(valueList.get(i));			
		}
		
		//((JavascriptExecutor)driver).executeScript("return validate('formA');");
		/*WebElement element = driver.findElement(By.id("search_btns_h"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);*/
		
		//((JavascriptExecutor)driver).executeScript("return validate('formA');");
		driver.findElement(By.id("search_btns_h")).click();
		firstSearch = (System.currentTimeMillis() / 1000);	
		
		//((JavascriptExecutor)driver).executeScript("$('#search_btns_h').click();");
		
		Thread.sleep(3000);
		
	
		return driver;
	}
	
	
	public ActivityDetails searchResults(WebDriver webDriver) throws InterruptedException, IOException{
		
		this.driver = webDriver;
				
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		WebDriverWait wait = new WebDriverWait(driver, 400);	
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("input-wrapper-submit")));
	
		activityDetails.setTimeFirstSearch(firstSearch);
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
		currentDate = format1.format(cal.getTime());
		
		SimpleDateFormat formatToDate = new SimpleDateFormat("dd-MMM-yyyy");
		String cDate = formatToDate.format(cal1.getTime());
		activityDetails.setCurrentDate(cDate);
		
		driver.switchTo().defaultContent();
		
		int activityCount = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='content-m']/div[2]/div/div[1]/div/div/p[2]/span")).getText());
		System.out.println("Activity count - " + activityCount);
		activityDetails.setActivityCount(activityCount);
		
		boolean resultsElement;
		
		File scrFile_2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile_2, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_ResultsPage.png"));
				
		if (driver.findElements(By.id("activity_results_container_0")).size() != 0) {
			resultsElement = true;
			activityDetails.setResultsAvailable(resultsElement);
			firstResultsAvail = (System.currentTimeMillis() / 1000);			
			activityDetails.setTimeFirstResults(firstResultsAvail);	
		}else{
			resultsElement = false;
			activityDetails.setResultsAvailable(resultsElement);
		}
		
		
		String showResAdd = driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).getText();
		activityDetails.setShowResultsAdd(showResAdd);
		
		driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
		Thread.sleep(500);
		
		File scrFile231 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile231, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_ResultsPage_AdditionalFilters.png"));
        Thread.sleep(500);
		
		
		if (driver.findElements(By.id("activityTypeId_a")).size() == 0) {
			activityDetails.setResultsProgramCat(false);
		}
		if (driver.findElements(By.id("AC_consumerCurrencyCode")).size() == 0) {
			activityDetails.setResultsPreCurrency(false);
		}
		if (driver.findElements(By.id("discountCoupon_No_H")).size() == 0) {
			activityDetails.setResultsPromoCode(false);
		}
		if (!(driver.findElement(By.xpath(".//*[@id='AC_consumerCurrencyCode']/option[2]")).getText().equalsIgnoreCase("AED"))) {
			activityDetails.setResultsCurrencyListLoaded(false);
		}		
		if (!(driver.findElement(By.xpath(".//*[@id='activityTypeId_a']/option[2]")).getText().equalsIgnoreCase("Transfer"))) {
			activityDetails.setResultsProgCatListLoaded(false);
		}
		
		
		String hideResAdd = driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).getText();
		activityDetails.setHideResultsAdd(hideResAdd);
		
		driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
		Thread.sleep(500);
		
		if (driver.findElements(By.id("price_slider_WJ_5")).size() == 0) {
			activityDetails.setPriceRangeFilter(false);
		}
		
		if (driver.findElements(By.id("booktype_text_WJ_8")).size() == 0) {
			activityDetails.setProgramAvailablity(false);
		}
		
		if (driver.findElements(By.id("name_text_WJ_4")).size() == 0) {
			activityDetails.setActivityNameFilter(false);
		}
		
		
		
	
		if (resultsElement == true) {
			
			int wholepages = (activityCount/10);
			
			if(((activityCount)%10) == 0){				
				System.out.println("Page count - " + wholepages);
			}else{
				wholepages ++;
				System.out.println("Page count - " + wholepages);
			}
			
		
			//Activity filter checking
			//Slider
			
			String minValue = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[2];
			String maxValue = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[6];
			activityDetails.setMinPrize(minValue);
			activityDetails.setMaxPrize(maxValue);
			
			//1
			String pageLoadedPrize = driver.findElement(By.xpath(".//*[@id='activity_results_container_0']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
			activityDetails.setPageLoadedResults(pageLoadedPrize);
						
			//2
			
			WebElement slider = driver.findElement(By.xpath(".//*[@id='price_slider_WJ_5']/a[1]"));
			Actions move = new Actions(driver);
			Action action = move.dragAndDropBy(slider, 10, 0).build();
			action.perform();
			Thread.sleep(1000);	
			
			String prizeChanged = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[2];
			activityDetails.setPrizeChanged_1(prizeChanged);
			String resultsPrize = driver.findElement(By.xpath(".//*[@id='activity_results_container_0']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
			activityDetails.setPrizeResults_1(resultsPrize);
					
			//3
			Actions move2 = new Actions(driver);
			Action action_2 = move2.dragAndDropBy(slider, 200, 0).build();
			action_2.perform();		
			
			String prizeChanged_100 = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[2];
			activityDetails.setPrizeChanged_2(prizeChanged_100);
			String resultsPrize_100 = driver.findElement(By.xpath(".//*[@id='activity_results_container_0']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
			activityDetails.setPrizeResults_2(resultsPrize_100);
			
			Thread.sleep(1500);	
			driver.findElement(By.xpath(".//*[@id='content-l']/div[2]/div[4]/a")).click();
			Thread.sleep(2500);	
			
			
			//On req
			
			Thread.sleep(2500);	
			new Select(driver.findElement(By.xpath(".//*[@id='booktype_text_WJ_8']"))).selectByValue("O");
			Thread.sleep(2500);	
			
			ArrayList<String> onReqLab = new ArrayList<String>();
			if (driver.findElements(By.className("result-block")).size() != 0) {
									
				List<WebElement> resultsblock = driver.findElements(By.className("result-block"));
				
				activityDetails.setResulteBlockSizeOnReq(resultsblock.size());
				
				for (int i = 0; i < resultsblock.size(); i++) {
					
					String labeltext = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/ul/li/p")).getText();
					onReqLab.add(labeltext);
				}
				
				activityDetails.setOnRequestLabels(onReqLab);
				
			}else{
				
				activityDetails.setResulteBlockSizeOnReq(1);
				onReqLab.add("On Request Activities Not Available");
				activityDetails.setOnRequestLabels(onReqLab);
				
			}
			
			
			//////
			
			Random ran = new Random();
			int x = ran.nextInt(100) + 5;
			
			if(x % 2 == 0 ){
				activityDetails.setAddAndContinue("true");
			}else{
				activityDetails.setAddAndContinue("true");
			}
			
			selectedDateExcel = searchInfo.getActivityDate();
			
			// select All activities	
			Thread.sleep(3500);	
			new Select(driver.findElement(By.xpath(".//*[@id='booktype_text_WJ_8']"))).selectByValue("B");
			Thread.sleep(2500);	
			
			//Price highest to lowest			
			driver.findElement(By.xpath(".//*[@id='content-m']/div[2]/div/div[2]/div/div/ul[1]/li/a[2]/i")).click();
			Thread.sleep(1000);	
			ArrayList<Integer> prizeListDescendingOrder = new ArrayList<Integer>();
			
			if (10 <= activityCount) {
				
				for (int j = 0; j < 10; j++) {
					
					int prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeListDescendingOrder.add(prizeForList);
					
				}
			}else{
				
				for (int j = 0; j < activityCount; j++) {
					
					int prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeListDescendingOrder.add(prizeForList);
					
				}
			}
			
			activityDetails.setBeforeSortingDescending(prizeListDescendingOrder);						
			Collections.sort(prizeListDescendingOrder, Collections.reverseOrder());
			activityDetails.setAfterSortingDescending(prizeListDescendingOrder);
			
			driver.findElement(By.xpath(".//*[@id='content-m']/div[2]/div/div[2]/div/div/ul[1]/li/a[1]/i")).click();
			Thread.sleep(2000);	
			
			///
			ArrayList<Integer> prizeList = new ArrayList<Integer>();
			
			if (10 <= activityCount) {
				
				for (int j = 0; j < 10; j++) {
					
					int prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeList.add(prizeForList);
					
					if (j==0) {
						
						if (activityDetails.getAddAndContinue().equals("true")) {
							
							WebElement mainElement = driver.findElement(By.id("activity_results_container_"+j+""));
							WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
							idForRemove = idElement.getAttribute("code");
													
							new Select(driver.findElement(By.xpath(".//*[@id='"+idForRemove+"_date_select']"))).selectByVisibleText(selectedDateExcel);
							Thread.sleep(1500);	
														
							driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[2]/a")).click();	
							Thread.sleep(5000);	
							
							wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div[2]/div[1]")));							
							WebElement selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div[2]/div[4]"));
							idNoFor2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];
							
							//checkbox
							driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_"+idForRemove+"_"+idNoFor2+"']")).click();
													
							//Add and Continue
											
							driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div[3]/div/a[2]")).click();
							Thread.sleep(5500);
							driver.findElement(By.xpath("html/body/div[7]/span/i")).click();
							Thread.sleep(3000);
							
							File scrFile_6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_6, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_AddToCart.png"));											        
									
					        if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_10']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setAddToCart(false);
							}
					        
							driver.findElement(By.xpath(".//*[@id='cart_display_WJ_10']/h4/div/span[2]/i")).click();
							Thread.sleep(3000);
																								
					        if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_10']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setActivityRemoved(false);
							}
					        
							driver.findElement(By.xpath("html/body/div[12]/div[11]/div/button[1]")).click();
							Thread.sleep(3000);
							
							File scrFile_7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_7, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_RemoveCart.png"));							
					        							
							driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[5]/input")).click();
							Thread.sleep(3000);
														
						}
					}	
					
					activityDetails.setBeforeSorting(prizeList);						
					Collections.sort(prizeList);
					activityDetails.setAfterSorting(prizeList);
					
				}
				
			}else{
				for (int j = 0; j < activityCount; j++) {
					
					int prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeList.add(prizeForList);
					
					if (j==0) {
						
						if (activityDetails.getAddAndContinue().equals("true")) {
							
							WebElement mainElement = driver.findElement(By.id("activity_results_container_"+j+""));
							WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
							idForRemove = idElement.getAttribute("code");
													
							new Select(driver.findElement(By.xpath(".//*[@id='"+idForRemove+"_date_select']"))).selectByVisibleText(selectedDateExcel);
							Thread.sleep(2500);	
							
							driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[2]/a")).click();	
							Thread.sleep(5000);	
							
							wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div[2]/div[1]")));							
							WebElement selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div[2]/div[4]"));
							idNoFor2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];
							
							//checkbox
							driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_"+idForRemove+"_"+idNoFor2+"']")).click();
													
							//Add and Continue
											
							driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div[3]/div/a[2]")).click();
							Thread.sleep(5500);
							driver.findElement(By.xpath("html/body/div[7]/span/i")).click();
							Thread.sleep(3000);
							
							File scrFile_6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_6, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_AddToCart.png"));											        
							
					        if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_10']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setAddToCart(false);
							}
					        
							driver.findElement(By.xpath(".//*[@id='cart_display_WJ_10']/h4/div/span[2]/i")).click();
							Thread.sleep(3000);
							
					        if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_10']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setActivityRemoved(false);
							}
					        
							driver.findElement(By.xpath("html/body/div[12]/div[11]/div/button[1]")).click();
							Thread.sleep(3000);
							
							File scrFile_7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_7, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_RemoveCart.png"));							
					      							
							driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[5]/input")).click();
							Thread.sleep(3000);
														
						}
						
					}	
					
					activityDetails.setBeforeSorting(prizeList);						
					Collections.sort(prizeList);
					activityDetails.setAfterSorting(prizeList);
					
				}
			}
			
			
			
			
			////////
			
			final short ACTIVITY_PER_PAGE=10;
			outerloop : for (int actvityPage = 1; actvityPage <= wholepages ; actvityPage++) {
				for (int i = 0 ; i < ACTIVITY_PER_PAGE; i++) {
					
					if ( ((actvityPage-1)*ACTIVITY_PER_PAGE) + (i) >= activityCount) {
						
						break outerloop;
						
					} else {
						
						String excelActivityName = searchInfo.getActivityName().replaceAll(" ", "");
						String webActivityName = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[1]/h1")).getText().replaceAll(" ", "");
						Thread.sleep(1000);
						
						WebElement elemSUP = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[2]"));
						String CheckSUP = elemSUP.findElement(By.tagName("a")).getAttribute("onclick");
						String supplierName = CheckSUP.split("'")[7];	
						
						if (supplierName.equalsIgnoreCase("rezbase_v3")) {
							rezAvtivity ++;
						}
						if (supplierName.equalsIgnoreCase("hbactivity")) {
							hbAvtivity ++;
						}
						
						if (supplierName.equalsIgnoreCase("gtaactivity")) {
							gtaActivity ++;
				
						}
						
						if (excelActivityName.equalsIgnoreCase(webActivityName)) {
							
							WebElement mainElement = driver.findElement(By.id("activity_results_container_"+i+""));
							WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
							idOnly = idElement.getAttribute("code");
							
							WebElement idElementwer = driver.findElement(By.xpath(".//*[@id='programridetracer_"+idOnly+"']"));
							traceID = idElementwer.getAttribute("value");
							activityDetails.setTraceId(traceID);
							
							WebElement daysElement = driver.findElement(By.id(""+idOnly+"_date_select"));
							List<WebElement> daysCountList = daysElement.findElements(By.tagName("option"));
							
							new Select(driver.findElement(By.xpath(".//*[@id='"+idOnly+"_date_select']"))).selectByVisibleText(selectedDateExcel);
							
							activityDetails.setDaysCount((daysCountList.size())-1);
							
							//Currency and Rate
							String activityCurr = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[2]/ul/li[1]/p/span[1]")).getText();
							activityDetails.setActivityCurrency(activityCurr);
							
							String mainRate = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
							activityDetails.setMainRate(mainRate);							
							Thread.sleep(500);
							
							String description = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[1]/div")).getText();
							activityDetails.setActDescription(description);
							
							//more details
							driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();	
							Thread.sleep(3000);
							
							File scrFile_55 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_55, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_MoreDetails.png"));							
					        
					        driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();	
							Thread.sleep(3000);
					
							/*//cancellation policy link
							
							driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[2]")).click();	
							Thread.sleep(3000);
							
							WebElement cancelElement = driver.findElement(By.className("cancellation-policy-body"));
							List<WebElement> rowsTo = cancelElement.findElements(By.tagName("li"));
							
							File scrFile_5 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_5, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_CancellationPolicy.png"));							
					        						
							ArrayList<String> policyList = new ArrayList<String>();
							
							for (int j = 1; j <= rowsTo.size()-1; j++) {
								
								String policy = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/section[2]/div/div/ul/li["+j+"]")).getText();
								policyList.add(policy);	
								
							}
							
							System.out.println(policyList);
							activityDetails.setCancelPolicy(policyList);
							
							driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/section[2]/a/i")).click();	
							Thread.sleep(1500);*/
							
						
							//////
							
							driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[2]/ul/li[2]/a")).click();	
							Thread.sleep(5000);	
							wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div[2]/div[1]")));														
							
							File scrFile_9 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_9, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_AvailabilityActivityTypes.png"));							
					       							
							ArrayList<String> excelActivityTypes = new ArrayList<String>();
							
							ArrayList<ActivityInfo> activityInfoList = searchInfo.getInventoryInfo();
							
							for (ActivityInfo activityInfo : activityInfoList) {
								
								String excelActString = activityInfo.getActivityType();
								excelActivityTypes.add(excelActString);	
							}
							
							
							List<WebElement> activityTypesElement = driver.findElements(By.className("activity-list-body"));
							divCount = activityTypesElement.size();
							
							ArrayList<String> resultsPage_ActivityTypes = new ArrayList<String>();
							ArrayList<String> resultsPage_City = new ArrayList<String>();
							ArrayList<String> resultsPage_Period = new ArrayList<String>();
							ArrayList<String> resultsPage_RateType = new ArrayList<String>();
							ArrayList<String> resultsPage_DailyRate = new ArrayList<String>();
							
							for (int j = 0; j < excelActivityTypes.size(); j++) {
								
								String excelActivityTypeName = excelActivityTypes.get(j).replaceAll(" ", "");
									
								for (int k = 1; k <= activityTypesElement.size(); k++) {
									
									String webActivityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[1]")).getText().replaceAll(" ", "");
									
									if (excelActivityTypeName.equalsIgnoreCase(webActivityTypeName)) {
										
										String rs_activityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[1]")).getText();
										resultsPage_ActivityTypes.add(rs_activityTypeName);
										
										String rs_activityPeriod = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[2]")).getText();
										resultsPage_Period.add(rs_activityPeriod);
										
										String rs_activityRate = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[3]")).getText();
										resultsPage_RateType.add(rs_activityRate);
										
										String rs_activityDailyRate = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[5]")).getText();
										resultsPage_DailyRate.add(rs_activityDailyRate);
										
										WebElement selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[4]"));
										idNo2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];
										
										int totalPaxCount = Integer.parseInt(searchInfo.getAdults()) + Integer.parseInt(searchInfo.getChildren());
										new Select(driver.findElement(By.xpath(".//*[@id='activity_qty_select_WJ_1_"+idOnly+"_"+idNo2+"']"))).selectByVisibleText(Integer.toString(totalPaxCount));
																			
										driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_"+idOnly+"_"+idNo2+"']")).click();
																		
									}
								}						
							}
							
							loadPayment = (System.currentTimeMillis() / 1000);			
							activityDetails.setTimeLoadpayment(loadPayment);
							
							activityDetails.setResultsPage_ActivityTypes(resultsPage_ActivityTypes);
							activityDetails.setResultsPage_Period(resultsPage_Period);
							activityDetails.setResultsPage_RateType(resultsPage_RateType);
							activityDetails.setResultsPage_DailyRate(resultsPage_DailyRate);
							
							if (activityDetails.getAddAndContinue().equals("true")) {
								
								//Add and Continue
												
								driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(divCount+2)+"]/div/a[2]")).click();
								Thread.sleep(5500);
								addedtoCart = (System.currentTimeMillis() / 1000);			
								activityDetails.setTimeAddedtoCart(addedtoCart);
								driver.findElement(By.xpath("html/body/div[7]/span/i")).click();
								Thread.sleep(2000);
								
								String cartCurrencyandValue = driver.findElement(By.xpath(".//*[@id='cart_display_WJ_10']/section/div")).getText();
								activityDetails.setCartVallue(cartCurrencyandValue);
								
							} 
														
						}
					}					
				}
				
				
				if (2 <= actvityPage) {
					driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span["+(actvityPage+2)+"]/a")).click();
				}else{
					driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span["+(actvityPage+1)+"]/a")).click();
				}
										
			}
			
			activityDetails.setGtaActivity(gtaActivity);
			activityDetails.setHbActivity(hbAvtivity);
			activityDetails.setRezActivity(rezAvtivity);
			

			Thread.sleep(2000);
			driver.findElement(By.xpath(".//*[@id='cart_display_WJ_10']/section/a")).click();
			Thread.sleep(2000);
					
			//Payment Page
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("payment-cart-sum-wrapper")));
			
			WebElement idStatusElement = driver.findElement(By.xpath("/html/body/section/form/div[1]/header/div/div/div[6]/div/div[2]/div/div[1]/ul[2]/li[2]/div"));
			IdStatus = idStatusElement.getAttribute("id").substring(15, 18);
			
			//Shopping Basket
			
			String shoppingCart_CartCurrency = driver.findElement(By.xpath(".//*[@id='sellingcurrency']")).getText();
			activityDetails.setShoppingCart_CartCurrency(shoppingCart_CartCurrency);
			
			String shoppingCart_subTotal = driver.findElement(By.xpath(".//*[@id='subtotal']")).getText();
			activityDetails.setShoppingCart_subTotal(shoppingCart_subTotal);
			String shoppingCart_TotalTax = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
			activityDetails.setShoppingCart_TotalTax(shoppingCart_TotalTax);
			String shoppingCart_TotalValue = driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText();
			activityDetails.setShoppingCart_TotalValue(shoppingCart_TotalValue);
			String shoppingCart_AmountNow = driver.findElement(By.xpath(".//*[@id='totalamountprocessnow']")).getText();
			activityDetails.setShoppingCart_AmountNow(shoppingCart_AmountNow);
			String shoppingCart_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='amountduetoutilazation']")).getText();
			activityDetails.setShoppingCart_AmountCheckIn(shoppingCart_AmountCheckIn);
			activityDetails.setPaymentDetails("Pay Online");
			
			
			//traveller details
			ArrayList<ThirdPaymentDetails> paymentInfoList = searchInfo.getPaymentDetailsInfo();
			
			for (ThirdPaymentDetails paymentDetails : paymentInfoList) {
				
				new Select(driver.findElement(By.id("cusTitle"))).selectByVisibleText(paymentDetails.getCustomerTitle());
				activityDetails.setPaymentPage_Title(paymentDetails.getCustomerTitle());
				
				driver.findElement(By.id("cusFName")).sendKeys(paymentDetails.getCustomerName());
				activityDetails.setPaymentPage_FName(paymentDetails.getCustomerName());
				driver.findElement(By.id("cusLName")).sendKeys(paymentDetails.getCustomerLastName());
				activityDetails.setPaymentPage_LName(paymentDetails.getCustomerLastName());
				driver.findElement(By.id("cusAdd1")).sendKeys(paymentDetails.getAddress());
				activityDetails.setPaymentPage_address(paymentDetails.getAddress());
				
				driver.findElement(By.id("cusCity")).sendKeys(paymentDetails.getCity());
				activityDetails.setPaymentPage_city(paymentDetails.getCity());
				
				String onLineTp_1 = paymentDetails.getTel().split("-")[0];
				String onLineTp_2 = paymentDetails.getTel().split("-")[1];
				
				driver.findElement(By.id("cusareacodetext")).sendKeys(onLineTp_1);
				driver.findElement(By.id("cusPhonetext")).sendKeys(onLineTp_2);
				activityDetails.setPaymentPage_TP(paymentDetails.getTel());
				
				driver.findElement(By.id("cusEmail")).sendKeys(paymentDetails.getEmail());
				activityDetails.setPaymentPage_Email(paymentDetails.getEmail());
				driver.findElement(By.id("cusConfEmail")).sendKeys(paymentDetails.getEmail());
				
				new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(paymentDetails.getCountry());	
				activityDetails.setPaymentPage_country(paymentDetails.getCountry());
				Thread.sleep(1500);
				
				if (paymentDetails.getCountry().equalsIgnoreCase("USA") || paymentDetails.getCountry().equalsIgnoreCase("Canada") || paymentDetails.getCountry().equalsIgnoreCase("Australia")) {
					
					if (driver.findElement(By.id("cusState")).isDisplayed() == true) {
						
						new Select(driver.findElement(By.id("cusState"))).selectByVisibleText(paymentDetails.getState());				
						activityDetails.setPaymentPage_State(paymentDetails.getState());
						Thread.sleep(1500);
						driver.findElement(By.id("cusZip")).sendKeys(paymentDetails.getPostalCode());
						activityDetails.setPaymentPage_PostalCode(paymentDetails.getPostalCode());
						
					}else{
						activityDetails.setPaymentPage_State("Not Available");
					}
								
				}	
				
			}
			
			// QUOTATION req
			
			if (searchInfo.getQuotationReq().equalsIgnoreCase("Yes")) {
			
				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[2]/div[2]/a[1]")).click();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("customername")));
				
				String Quote_QuotationNo = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[1]/ul/li[3]/p[2]/b")).getText();
				activityDetails.setQuote_QuotationNo(Quote_QuotationNo);
				
				String Quote_Mail = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[1]/ul/li[5]/p/span")).getText();
				activityDetails.setQuote_Mail(Quote_Mail);
				
				String Quote_Status = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[2]/div[1]/div/p[2]")).getText();
				activityDetails.setQuote_Status(Quote_Status);
				
				String Quote_Currency = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/ul/li[1]/div")).getText();
				activityDetails.setQuote_Currency(Quote_Currency);
				
				String Quote_QTY = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[3]/ul[2]/li[3]")).getText();
				activityDetails.setQuote_QTY(Quote_QTY);
				
				String Quote_ActivityName = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[1]/p[1]")).getText();
				activityDetails.setQuote_ActivityName(Quote_ActivityName);
				
				String Quote_CityItinary = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[1]/p[2]")).getText();
				activityDetails.setQuote_CityItinary(Quote_CityItinary);
				
				String Quote_ActivityTypeName = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[1]/p[3]")).getText();
				activityDetails.setQuote_ActivityTypeName(Quote_ActivityTypeName);
				
				////
				
				String Quote_FName = driver.findElement(By.xpath(".//*[@id='customerfname']")).getText().split(": ")[1];
				activityDetails.setQuote_FName(Quote_FName);
				
				String Quote_LName = driver.findElement(By.xpath(".//*[@id='customerlname']")).getText().split(": ")[1];
				activityDetails.setQuote_LName(Quote_LName);
				
				String Quote_TP = driver.findElement(By.xpath(".//*[@id='cusemergencycontact']")).getText().split(": ")[1];
				activityDetails.setQuote_TP(Quote_TP);
				
				String Quote_Email = driver.findElement(By.xpath(".//*[@id='customeremail']")).getText().split(": ")[1];
				activityDetails.setQuote_Email(Quote_Email);
				
				String Quote_address_1 = driver.findElement(By.xpath(".//*[@id='customeraddress1']")).getText().split(": ")[1];
				activityDetails.setQuote_address_1(Quote_address_1);
				
								
				String Quote_city = driver.findElement(By.xpath(".//*[@id='customercity']")).getText().split(": ")[1];
				activityDetails.setQuote_city(Quote_city);
				
				String Quote_country = driver.findElement(By.xpath(".//*[@id='customercountry']")).getText().split(": ")[1];
				activityDetails.setQuote_country(Quote_country);
				
				if (Quote_country.equalsIgnoreCase("USA")) {
					
					String Quote_state = driver.findElement(By.xpath(".//*[@id='customerstate']")).getText().split(": ")[1];
					activityDetails.setQuote_state(Quote_state);
					
					String Quote_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText().split(": ")[1];
					activityDetails.setQuote_postalCode(Quote_postalCode);
				}
				
				if (Quote_country.equalsIgnoreCase("Canada")) {
					
					String Quote_state = driver.findElement(By.xpath(".//*[@id='customerstate']")).getText().split(": ")[1];
					activityDetails.setQuote_state(Quote_state);
					
					String Quote_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText().split(": ")[1];
					activityDetails.setQuote_postalCode(Quote_postalCode);
				}
				
				if (Quote_country.equalsIgnoreCase("Australia")) {
					
					String Quote_state = driver.findElement(By.xpath(".//*[@id='customerstate']")).getText().split(": ")[1];
					activityDetails.setQuote_state(Quote_state);
					
					String Quote_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText().split(": ")[1];
					activityDetails.setQuote_postalCode(Quote_postalCode);
				}
				
				
				
				String Quote_subTotal = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[5]/ul/li[2]/div[2]")).getText();
				activityDetails.setQuote_subTotal(Quote_subTotal);
				
				String Quote_taxandOther = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[5]/ul/li[3]/div[2]")).getText();
				activityDetails.setQuote_taxandOther(Quote_taxandOther);
				
				String Quote_totalValue = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[5]/ul/li[4]/div[2]")).getText();
				activityDetails.setQuote_totalValue(Quote_totalValue);
				
				
				String Quote_TotalGrossPackageValue = driver.findElement(By.xpath(".//*[@id='totalbeforetax']")).getText();
				activityDetails.setQuote_TotalGrossPackageValue(Quote_TotalGrossPackageValue);
				
				String Quote_TotalTax = driver.findElement(By.xpath(".//*[@id='tottaxandothercharges']")).getText();
				activityDetails.setQuote_TotalTax(Quote_TotalTax);
				
				String Quote_TotalPackageValue = driver.findElement(By.xpath(".//*[@id='totalpayable']")).getText();
				activityDetails.setQuote_TotalPackageValue(Quote_TotalPackageValue);
				
							
			}else{
				
				//No Quotatin
				
				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[2]/div[2]/a[2]")).click();
				Thread.sleep(1000);
				
				//Occupancy Details
				
				Random rand = new Random(); 
				int randNumber = rand.nextInt(1000) + 5;
				ArrayList<String> cusTitle = new ArrayList<String>();
				ArrayList<String> cusFName = new ArrayList<String>();
				ArrayList<String> cusLname = new ArrayList<String>();
				
				WebElement cancelElementID = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[4]/div[1]/div/div/div/input"));
				cancelID = cancelElementID.getAttribute("value");
								
				List<WebElement> occupancyListElement = driver.findElements(By.className("child-title"));
				String actSelectedDate = searchInfo.getActivityDate();
				//title_2029~2079~0~23-Jan-2015
				
				for (int i = 0; i < occupancyListElement.size(); i++) {
					
					String title;
					
					try {
						title = "Mr";
						new Select(driver.findElement(By.id("title_"+IdStatus+"0~"+i+"~"+actSelectedDate+""))).selectByVisibleText(title);
						cusTitle.add(title);
						
					} catch (Exception e) {
						title = "Mst.";
						new Select(driver.findElement(By.id("title_"+IdStatus+"0~"+i+"~"+actSelectedDate+""))).selectByVisibleText(title);
						cusTitle.add(title);
					}
					
					
					String fName = "FirstNamePer";
					driver.findElement(By.xpath(".//*[@id='firstName_"+IdStatus+"0~"+i+"~"+actSelectedDate+"']")).sendKeys(fName);
					cusFName.add(fName);
						
					String lName = "LastNamePer";
					driver.findElement(By.xpath(".//*[@id='lastName_"+IdStatus+"0~"+i+"~"+actSelectedDate+"']")).sendKeys(lName);
					cusLname.add(lName);
					
				}
				
				activityDetails.setResultsPage_cusTitle(cusTitle);
				activityDetails.setResultsPage_cusFName(cusFName);
				activityDetails.setResultsPage_cusLName(cusLname);
				
				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[4]/div[2]/a[2]")).click();
				Thread.sleep(1000);
				
				//Billing Info
				
				String paymentBilling_CardTotal = driver.findElement(By.xpath(".//*[@id='totchgpgamt']")).getText();
				activityDetails.setPaymentBilling_CardTotal(paymentBilling_CardTotal);
				String paymentBilling_CardCurrency = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[6]/div[3]/div/div[2]/ul/li[1]/div")).getText();
				activityDetails.setPaymentBilling_CardCurrency(paymentBilling_CardCurrency);
				
				String paymentBilling_subTotal = driver.findElement(By.xpath(".//*[@id='paymentdetails_subtotal']")).getText();
				activityDetails.setPaymentBilling_subTotal(paymentBilling_subTotal);
				String paymentBilling_TotalTax = driver.findElement(By.xpath(".//*[@id='totaltaxpgcurrid']")).getText();
				activityDetails.setPaymentBilling_TotalTax(paymentBilling_TotalTax);
				String paymentBilling_TotalValue = driver.findElement(By.xpath(".//*[@id='totalchargeamtpgcurrid']")).getText();
				activityDetails.setPaymentBilling_TotalValue(paymentBilling_TotalValue);
				String paymentBilling_AmountNow = driver.findElement(By.xpath(".//*[@id='totalpaynowamountpgcurrid']")).getText();
				activityDetails.setPaymentBilling_AmountNow(paymentBilling_AmountNow);
				String paymentBilling_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='totalamountatutilizationpgcurrid']")).getText();
				activityDetails.setPaymentBilling_AmountCheckIn(paymentBilling_AmountCheckIn);
				
				Thread.sleep(1000);
				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[6]/div[4]/a[2]")).click();
				Thread.sleep(2000);
				
				//Notes
				
				String cusNotes = "I am wanting to generate a random string of 20 characters without " +
						"using apache classes. I don't really care about whether is " +
						"alphanumeric or not. Also, I am going to convert it to an array of bytes later FYI";
				driver.findElement(By.id("txt_cus_notes")).sendKeys(cusNotes);
				activityDetails.setCustomerNotes(cusNotes);
				
				String actNotes = "Activity notes displaying here.";
				driver.findElement(By.xpath(".//*[@id='activitynotesid0']/textarea")).sendKeys(actNotes);
				activityDetails.setActivityNotes(actNotes);
				Thread.sleep(1000);
				
				driver.findElement(By.xpath(".//*[@id='paynotes']/div[3]/a[2]")).click();
				Thread.sleep(1000);
				
				// Terms and condition
				
				WebElement cancelElementlistPayment = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_"+cancelID+"']/div[3]"));
				List<WebElement> liElementPay = cancelElementlistPayment.findElements(By.tagName("li"));
				
				ArrayList<String> cancelListPay = new ArrayList<String>();
				
				for (int i = 1; i <= liElementPay.size(); i++) {
					
					String cancelPolicies = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_"+cancelID+"']/div[3]/ul/li["+i+"]")).getText();
					cancelListPay.add(cancelPolicies);
					
				}
				
				activityDetails.setPaymentCancelPolicy(cancelListPay);
				
			
				driver.findElement(By.xpath(".//*[@id='confreadPolicy']")).click();
				driver.findElement(By.xpath(".//*[@id='confTnC']")).click();
				
				driver.findElement(By.xpath(".//*[@id='confirm-payment-btn']")).click();			
				Thread.sleep(4500);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='divConfirm']/a")));
				
											
				//Popup window
				
				String paymentPopUp_ActivityName = driver.findElement(By.xpath(".//*[@id='header']/div[6]/div/div[2]/div/div[1]/ul[2]/li[1]/div")).getText();
				activityDetails.setPaymentPopUp_ActivityName(paymentPopUp_ActivityName);
							
				driver.findElement(By.xpath(".//*[@id='divConfirm']/a")).click();
				availablClick = (System.currentTimeMillis() / 1000);			
				activityDetails.setTimeAailableClick(availablClick);
				Thread.sleep(5000);
				
				//Payment gateway
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paygatewayFrame")));
				driver.switchTo().frame("paygatewayFrame");
				
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("document.getElementById('cardnumberpart1').value = '4000';");
				jse.executeScript("document.getElementById('cardnumberpart2').value = '0000';");
				jse.executeScript("document.getElementById('cardnumberpart3').value = '0000';");
				jse.executeScript("document.getElementById('cardnumberpart4').value = '0002';");
			
				new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText("01");
				new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText("2016");
				
				driver.findElement(By.xpath(".//*[@id='cardholdername']")).sendKeys("Asiri");
				jse.executeScript("document.getElementById('cv2').value = '123';");
				
				String amount = driver.findElement(By.xpath(".//*[@id='main_dev']/div[7]/div[2]")).getText();
				activityDetails.setPaymentGatewayTotal(amount);
							
				driver.findElement(By.xpath(".//*[@id='main_dev']/div[8]/div[2]/a/div")).click();	
				Thread.sleep(7000);
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame("paygatewayFrame");
				
				try {
					driver.findElement(By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).sendKeys("1234");
					
				} catch (Exception e) {
					driver.findElement(By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).sendKeys("1234");				
				}
				
				driver.findElement(By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[4]/td[2]/b/input")).click();	
				Thread.sleep(5000);
				
				
				
				//Confirmation Page
				
				driver.switchTo().defaultContent();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.className("confirmation-block-wrapper")));	
				
				String referenceName = driver.findElement(By.xpath(".//*[@id='customername']")).getText().split(": ")[1];
				activityDetails.setConfirmation_BookingRefference(referenceName);
				String bookingNo = driver.findElement(By.xpath(".//*[@id='activityresno']")).getText();
				comfirmBooking = (System.currentTimeMillis() / 1000);			
				activityDetails.setTimeConfirmBooking(comfirmBooking);
				activityDetails.setReservationNo(bookingNo);
				String confirmation_CusMailAddress = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[1]/ul/li[4]/p/span")).getText();
				activityDetails.setConfirmation_CusMailAddress(confirmation_CusMailAddress);
				
				
				String confirmation_BI_ActivityName = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[1]/p[1]")).getText();
				activityDetails.setConfirmation_BI_ActivityName(confirmation_BI_ActivityName);
				
				String confirmation_BI_City = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[1]/p[2]")).getText();
				activityDetails.setConfirmation_BI_City(confirmation_BI_City);
				
				String confirmation_BI_ActType = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[1]/p[3]")).getText();
				activityDetails.setConfirmation_BI_ActType(confirmation_BI_ActType);
				
				String confirmation_BI_BookingStatus = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[2]/div[1]/div/p[2]")).getText();
				activityDetails.setConfirmation_BI_BookingStatus(confirmation_BI_BookingStatus);
				
				String confirmation_BI_SelectedDate = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[2]/div[2]/div/p[2]")).getText();
				activityDetails.setConfirmation_BI_SelectedDate(confirmation_BI_SelectedDate);
				///
				
				ArrayList<String> confirmationPage_RateType = new ArrayList<String>();
				ArrayList<String> confirmationPage_DailyRate = new ArrayList<String>();
				ArrayList<String> confirmationPage_QTY = new ArrayList<String>();
				
				List<WebElement> confirmationActlist = driver.findElements(By.className("activity-table-body"));
				
				for (int i = 1; i <= confirmationActlist.size(); i++) {
					
					String confirmationRates = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[3]/ul["+(i+1)+"]/li[1]")).getText();
					confirmationPage_RateType.add(confirmationRates);
					
					String confirmationCost = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[3]/ul["+(i+1)+"]/li[2]")).getText();
					confirmationPage_DailyRate.add(confirmationCost);
					
					String confirmationQTY = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[3]/ul["+(i+1)+"]/li[3]")).getText();
					confirmationPage_QTY.add(confirmationQTY);
					
				}
				
				activityDetails.setConfirmation_RateType(confirmationPage_RateType);
				activityDetails.setConfirmation_DailyRate(confirmationPage_DailyRate);
				activityDetails.setConfirmation_QTY(confirmationPage_QTY);
				
				////
				
				String confirmation_Currency1 = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[4]/ul/li[1]/div")).getText();
				activityDetails.setConfirmation_Currency1(confirmation_Currency1);		
				String confirmation_Currency2 = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/div[1]/ul/li[1]/div")).getText();
				activityDetails.setConfirmation_Currency2(confirmation_Currency2);	
				
				
				
				
				String confirmation_SubTotal = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[5]/ul/li[2]/div[2]")).getText();
				activityDetails.setConfirmation_SubTotal(confirmation_SubTotal);	
				
				String confirmation_Tax = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[5]/ul/li[3]/div[2]")).getText();
				activityDetails.setConfirmation_Tax(confirmation_Tax);	
				
				String confirmation_Total = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[2]/div[5]/ul/li[4]/div[2]")).getText();
				activityDetails.setConfirmation_Total(confirmation_Total);	
				
				
				
				String confirmation_SubTotal2 = driver.findElement(By.xpath(".//*[@id='totalbeforetax']")).getText();
				activityDetails.setConfirmation_SubTotal2(confirmation_SubTotal2);							
				
				String confirmation_TotalTaxOtherCharges = driver.findElement(By.xpath(".//*[@id='tottaxandothercharges']")).getText();
				activityDetails.setConfirmation_TotalTaxOtherCharges(confirmation_TotalTaxOtherCharges);	
				
				String confirmation_Total2 = driver.findElement(By.xpath(".//*[@id='totalpayable']")).getText();
				activityDetails.setConfirmation_Total2(confirmation_Total2);	
				
				String confirmation_AmountNow = driver.findElement(By.xpath(".//*[@id='amountbeingprocessednow']")).getText();
				activityDetails.setConfirmation_AmountNow(confirmation_AmountNow);	
				
				String confirmation_AmountDueCheckIn = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
				activityDetails.setConfirmation_AmountDueCheckIn(confirmation_AmountDueCheckIn);	
				
				//////
							
				String confirmation_FName = driver.findElement(By.xpath(".//*[@id='customerfname']")).getText().split(": ")[1];
				activityDetails.setConfirmation_FName(confirmation_FName);
				
				String confirmation_LName = driver.findElement(By.xpath(".//*[@id='customerlname']")).getText().split(": ")[1];
				activityDetails.setConfirmation_LName(confirmation_LName);
				
				String confirmation_TP; 			
				try {
					confirmation_TP = driver.findElement(By.xpath(".//*[@id='cusemergencycontact']")).getText().split(": ")[1];
					activityDetails.setConfirmation_TP(confirmation_TP);
				} catch (Exception e) {
					confirmation_TP = "Not Available";
					activityDetails.setConfirmation_TP(confirmation_TP);
				}
			
				String confirmation_Email = driver.findElement(By.xpath(".//*[@id='customeremail']")).getText().split(": ")[1];
				activityDetails.setConfirmation_Email(confirmation_Email);
				
				String confirmation_address = driver.findElement(By.xpath(".//*[@id='customeraddress1']")).getText().split(": ")[1];
				activityDetails.setConfirmation_address(confirmation_address);
				
				String confirmation_country = driver.findElement(By.xpath(".//*[@id='customercountry']")).getText().split(": ")[1];
				activityDetails.setConfirmation_country(confirmation_country);
				
				String confirmation_city = driver.findElement(By.xpath(".//*[@id='customercity']")).getText().split(": ")[1];
				activityDetails.setConfirmation_city(confirmation_city);
				
				String confirmation_State = driver.findElement(By.xpath(".//*[@id='customerstate']")).getText().split(": ")[1];
				activityDetails.setConfirmation_State(confirmation_State);
				
				String confirmation_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText().split(": ")[1];
				activityDetails.setConfirmation_postalCode(confirmation_postalCode);
				
				
				
				////confirmation_postalCode
				
				String confirmation_AODActivityName = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[3]/h3")).getText().split(" - ")[1];
				activityDetails.setConfirmation_AODActivityName(confirmation_AODActivityName);
				
				String confirmation_BillingInfo_TotalValue = driver.findElement(By.xpath(".//*[@id='paymentamount']")).getText();
				activityDetails.setConfirmation_BillingInfo_TotalValue(confirmation_BillingInfo_TotalValue);
				
				ArrayList<String> confirmTitle = new ArrayList<String>();
				ArrayList<String> confirmFirstName = new ArrayList<String>();
				ArrayList<String> confirmLastName = new ArrayList<String>();
				
				List<WebElement> confirmationPaxlist = driver.findElements(By.className("car-details-body"));
				
				for (int i = 1; i <= confirmationPaxlist.size(); i++) {
					
					String confirmationT = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[3]/div[3]/div/ul["+(i+1)+"]/li[2]")).getText();
					confirmTitle.add(confirmationT);
					
					String confirmationFN = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[3]/div[3]/div/ul["+(i+1)+"]/li[3]")).getText();
					confirmFirstName.add(confirmationFN);
					
					String confirmationLN = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[3]/div[3]/div/ul["+(i+1)+"]/li[4]")).getText();
					confirmLastName.add(confirmationLN);
					
				}
				
				activityDetails.setConfirmationPage_cusTitle(confirmTitle);
				activityDetails.setConfirmationPage_cusFName(confirmFirstName);
				activityDetails.setConfirmationPage_cusLName(confirmLastName);
				
				///
				
				WebElement cancelElementlist = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_"+idOnly+"']/div[3]"));
				List<WebElement> liElement = cancelElementlist.findElements(By.tagName("li"));
				
				ArrayList<String> cancelList = new ArrayList<String>();
				
				for (int i = 1; i <= liElement.size(); i++) {
					
					String cancelPolicies = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_"+idOnly+"']/div[3]/ul/li["+i+"]")).getText();
					cancelList.add(cancelPolicies);
					
				}
				
				activityDetails.setConfirmCancelPolicy(cancelList);
				
								
			}
			
			
		}
		
		return activityDetails;
	}
	
	
	
	
}
