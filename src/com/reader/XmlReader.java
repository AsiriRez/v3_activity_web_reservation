package com.reader;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.model.*;

public abstract class XmlReader {
	
	public WebDriver driver;
	public String traceID;
	
	
	public XmlReader(WebDriver Newdriver){
	    driver = Newdriver;
	}
	
	
	public AvailabilityRequest readAvailabilityRequests(){
			
		AvailabilityRequest a_Request  = new AvailabilityRequest();
				
		return a_Request;
	}
	
	public AvailabilityResponse readAvailabilityResponses(){
		
		AvailabilityResponse a_Response  = new AvailabilityResponse();
		
		return a_Response;		
	}
	
	public ProgramRequest getProgramRequests(){
		
		ProgramRequest p_Request = new ProgramRequest();
		
		return p_Request;		
	}
	
	public ProgramResponse getProgramResponses(){
		
		ProgramResponse p_Response = new ProgramResponse();
		
		return p_Response;			
	}
	
	public PreservationRequest getReservationRequests(){
		
		PreservationRequest r_Request = new PreservationRequest();
		
		return r_Request;
	}
	
	public PreReservationResponse getReservationResponses(){
		
		PreReservationResponse r_Response = new PreReservationResponse();
		
		return r_Response;
	}

	public CancellationPolicyRequest getCancellationPolicyRequests(){
		
		CancellationPolicyRequest cp_req = new CancellationPolicyRequest();
		
		return cp_req;
		
	}
	
	
	public CancellationPolicyResponse getCancellationPolicyResponse(){
		
		CancellationPolicyResponse cp_response = new CancellationPolicyResponse();
		
		return cp_response;
		
		
	}
	
	public String getWindow(String partialLink, WebDriver Driver, String traceId) throws InterruptedException
	{		
		String WindowHandle = driver.getWindowHandle();
		
		Driver.findElement(By.partialLinkText(partialLink.concat(traceId))).click();
	    
		ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
		list.remove(WindowHandle);
		driver.switchTo().window(list.get(0));
		
		return WindowHandle;
		
	}


	

	
	
}
