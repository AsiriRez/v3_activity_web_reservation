package com.reader;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;

import com.controller.PG_Properties;
import com.model.ActivityDetails;
import com.model.ActivityInfo;
import com.model.Search;
import com.types.TourOperatorType;

public class TORatesLogics {
	
	private WebDriver driver = null;
	private ActivityDetails activityDetails;
	private Search search_Info;
	private ActivityInfo inventoryListss;
	private int paxCnt, discountedVale;
	private double sellRate;
	private double netRate;
	private String ToActivityPM, discountYesNo, ToActivityNetRate, ToActivity_SalesTaxType, ToActivity_SalesTaxValue, ToActivity_MisFeeType, ToActivity_MisFeeValue;
	private TourOperatorType UserTypeDC_B2B;
	
	
	public TORatesLogics(Search search){
		this.search_Info = search;
		
	}
	
	public ActivityDetails getTORatesCalculation(){
		
		activityDetails = new ActivityDetails();
		
		ArrayList<ActivityInfo> activityList = search_Info.getInventoryInfo();
		
		for (ActivityInfo activityInfo : activityList) {
			
			ToActivityPM = activityInfo.getToActivityPM();  
			ToActivityNetRate = activityInfo.getToActivityNetRate(); 
			ToActivity_SalesTaxType = activityInfo.getToActivity_SalesTaxType(); 
			ToActivity_SalesTaxValue = activityInfo.getToActivity_SalesTaxValue(); 
			ToActivity_MisFeeType = activityInfo.getToActivity_MisFeeType(); 
			ToActivity_MisFeeValue = activityInfo.getToActivity_MisFeeValue(); 
			UserTypeDC_B2B = activityInfo.getUserTypeDC_B2B();
			discountYesNo = activityInfo.getDiscountYesORNo();
			
		}
				
		if (! (UserTypeDC_B2B == TourOperatorType.DC)) {
						
			double totalNetRate_SalesTax = 0;
			double totalNetRate_MisFees = 0;
			
			paxCnt = Integer.parseInt(search_Info.getAdults()) + Integer.parseInt(search_Info.getChildren());		
			double pm = Double.parseDouble(ToActivityPM);
			netRate = Double.parseDouble(ToActivityNetRate);
			
			sellRate = (netRate* ((100 + pm) / 100)) * paxCnt;	
			double totalNetRate = (netRate * paxCnt);
					
			if (ToActivity_SalesTaxType.equalsIgnoreCase("percentage")) {
				
				double salesTax = Double.parseDouble(ToActivity_SalesTaxValue);
				totalNetRate_SalesTax = (totalNetRate* ((salesTax) / 100));				
			}
			
			if (ToActivity_SalesTaxType.equalsIgnoreCase("value")) {			
				double salesTax = Double.parseDouble(ToActivity_SalesTaxValue);
				totalNetRate_SalesTax = totalNetRate + salesTax;
			}
			
			if (ToActivity_MisFeeType.equalsIgnoreCase("percentage")) {
				
				double misFees = Double.parseDouble(ToActivity_MisFeeValue);
				totalNetRate_MisFees = (totalNetRate* ((misFees) / 100));				
			}
			
			if (ToActivity_MisFeeType.equalsIgnoreCase("value")) {			
				double misFees = Double.parseDouble(ToActivity_MisFeeValue);
				totalNetRate_MisFees = totalNetRate + misFees;
			}
			
			int subTotal = (int) Math.ceil(sellRate);
			int totalTax = (int) Math.ceil(totalNetRate_SalesTax + totalNetRate_MisFees);
			
								
			if (UserTypeDC_B2B == TourOperatorType.COMCASH || UserTypeDC_B2B == TourOperatorType.COMCREDITLPONO || UserTypeDC_B2B == TourOperatorType.COMCREDITLPOY) {
				
				int agentCommssion = 0;
				String toAgentTypePerOrValue = PG_Properties.getProperty(UserTypeDC_B2B.toString() + "#Type");
				
				if (toAgentTypePerOrValue.equalsIgnoreCase("percentage")) {
					
					double agentPercentage = Double.parseDouble(PG_Properties.getProperty(UserTypeDC_B2B.toString() + "#Value"));
					agentCommssion = (int)(sellRate* ((agentPercentage) / 100));	

				} else {
					
					double agentCommValue = Double.parseDouble(PG_Properties.getProperty(UserTypeDC_B2B.toString() + "#Value"));
					agentCommssion = (int)(sellRate + agentCommValue) ;	

				}
					

				activityDetails.setToAgentCommission(Integer.toString((int)Math.ceil(agentCommssion)));
				
			}
			
			int disValue = 0;
			int TotalValue = 0;
			
			if (discountYesNo.equalsIgnoreCase("Yes")) {
				
				if (PG_Properties.getProperty("Discount_Type").equalsIgnoreCase("percentage")) {
					
					disValue = Integer.parseInt(PG_Properties.getProperty("Discount_Value"));
					discountedVale = ((subTotal * disValue)/100);							
				}
				
				if (PG_Properties.getProperty("Discount_Type").equalsIgnoreCase("value")) {			
					
					disValue = Integer.parseInt(PG_Properties.getProperty("Discount_Value"));
					discountedVale = subTotal + disValue;
				}
				
				TotalValue = (int) Math.ceil(subTotal + totalTax - discountedVale);
				
				
			}else{
				
				TotalValue = (int) Math.ceil(subTotal + totalTax);
			}
			
										
			activityDetails.setToActivity_SubTotal(Double.toString(subTotal));
			activityDetails.setToActivity_TotalTax(Double.toString(totalTax));
			activityDetails.setToActivity_TotalValue(Double.toString(TotalValue));
				
		}
					
		return activityDetails;
		
	}
	

}
