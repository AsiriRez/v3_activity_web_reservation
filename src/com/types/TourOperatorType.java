package com.types;

public enum TourOperatorType {

	DC,NETCASH, NETCREDITLPOY, NETCREDITLPON, COMCASH, COMCREDITLPOY, COMCREDITLPONO, NONE;

	public static TourOperatorType getTourOperatorType(String touroperator) {

		if (touroperator.trim().equalsIgnoreCase("WEB_NETCASH"))
			return TourOperatorType.NETCASH;
		else if (touroperator.trim().equalsIgnoreCase("WEB_NETCREDITLPOY"))
			return TourOperatorType.NETCREDITLPOY;
		else if (touroperator.trim().equalsIgnoreCase("WEB_NETCREDITLPON"))
			return TourOperatorType.NETCREDITLPON;
		else if (touroperator.trim().equalsIgnoreCase("WEB_COMCASH"))
			return TourOperatorType.COMCASH;
		else if (touroperator.trim().equalsIgnoreCase("WEB_COMCREDITLPOY"))
			return TourOperatorType.COMCREDITLPOY;
		else if (touroperator.trim().equalsIgnoreCase("WEB_COMCREDITLPONO"))
			return TourOperatorType.COMCREDITLPONO;
		else if (touroperator.trim().equalsIgnoreCase("WEB_DC"))
			return TourOperatorType.DC;
		else
			return TourOperatorType.NONE;
	}

}
