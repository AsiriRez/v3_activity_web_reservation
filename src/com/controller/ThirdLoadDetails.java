package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.model.ActivityInfo;
import com.model.PaymentDetails;
import com.model.Search;
import com.model.ThirdPaymentDetails;
import com.model.ThirdSearch;


public class ThirdLoadDetails {
	
	Map<Integer, String> SearchMap 		= null;
	Map<Integer, String> CustomerMap 	= null;
	Map<Integer, String> InventoryTypeMap 	= null;

	public ThirdLoadDetails(ArrayList<Map<Integer, String>> sheetlist){
		
		SearchMap          		= sheetlist.get(0);
		CustomerMap          	= sheetlist.get(1);
		InventoryTypeMap		= sheetlist.get(2);
	}
	
	public TreeMap<String, ThirdSearch> loadActivityReservation(Map<Integer, String> searchInfoDeMap){
		
		Iterator<Map.Entry<Integer, String>> it = searchInfoDeMap.entrySet().iterator();
		TreeMap<String, ThirdSearch> activitySearchInfoMap = new TreeMap<String, ThirdSearch>();
		
		while(it.hasNext()) {
			
			ThirdSearch search = new ThirdSearch();
			String[] values = it.next().getValue().split(",");
			
			search.setScenarioCount(values[0]);
			search.setSellingCurrency(values[1]);
			search.setCountry(values[2]);
			search.setDestination(values[3]);
			search.setDateFrom(values[4]);
			search.setDateTo(values[5]);
			search.setAdults(values[6]);
			search.setChildren(values[7]);
			search.setAgeOfChildren(values[8]);
			search.setProgramCategory(values[9]);
			search.setPreferCurrency(values[10]);
			search.setPromotionCode(values[11]);
			search.setActivityName(values[12]);
			search.setActivityDate(values[13]);
			search.setScenariotype(values[14]);
			search.setBooking_channel(values[15]);
			search.setQuotationReq(values[16]);
			
			activitySearchInfoMap.put(values[0], search);
		}
		
		return activitySearchInfoMap;
	}	
	
	public TreeMap<String, ThirdSearch> loadPaymentDetails(Map<Integer, String> paymentsMapDe, TreeMap<String, ThirdSearch> searchInfoDeMap) {

		Iterator<Map.Entry<Integer, String>> it = paymentsMapDe.entrySet().iterator();
		
		while (it.hasNext()) {
			
			ThirdPaymentDetails paymentDetails = new ThirdPaymentDetails();
			String[] values = it.next().getValue().split(",");

			paymentDetails.setScenarioId(values[0]);
			paymentDetails.setCustomerTitle(values[1]);
			paymentDetails.setCustomerName(values[2]);
			paymentDetails.setCustomerLastName(values[3]);
			paymentDetails.setTel(values[4]);
			paymentDetails.setEmail(values[5]);
			paymentDetails.setAddress(values[6]);
			paymentDetails.setAddress_1(values[7]);
			paymentDetails.setCountry(values[8]);
			paymentDetails.setCity(values[9]);
			paymentDetails.setState(values[10]);
			paymentDetails.setPostalCode(values[11]);
						
			try {				
				searchInfoDeMap.get(values[0]).addPaymentInfo(paymentDetails);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return searchInfoDeMap;
	}
	
	
	public TreeMap<String, ThirdSearch> loadActivityInventoryRecords(Map<Integer, String> inventoryMap, TreeMap<String, ThirdSearch> searchInfoDeMap) {
		
		Iterator<Map.Entry<Integer, String>> it = inventoryMap.entrySet().iterator();
		
		while (it.hasNext()) {
			
			ActivityInfo activityInventory = new ActivityInfo();
			String[] values = it.next().getValue().split(",");
			
			activityInventory.setScenarioCount(values[0]);
			activityInventory.setActivityName(values[1]);
			activityInventory.setSupplier_Currency(values[2]);
			activityInventory.setActivityType(values[3]);
			
			activityInventory.setPaymentPage_SubTotal(values[4]);	
			activityInventory.setPaymentPage_TotalTaxnOther(values[5]);	
			activityInventory.setCreditCardFee(values[6]);
			activityInventory.setPaymentPage_Total(values[7]);		
			activityInventory.setPaymentPage_AmountNow(values[8]);		
			activityInventory.setPaymentPage_AmountDue(values[9]);	
			
			
			activityInventory.setProfir_Markup(values[10]);	
			activityInventory.setStandardCancell_Type(values[11]);
			activityInventory.setStandardCancell_Value(values[12]);	
			activityInventory.setNoShow_Type(values[13]);
			activityInventory.setNoShow_Value(values[14]);
			
			activityInventory.setApplyIfLessThan(values[15]);	
			activityInventory.setCancelBuffer(values[16]);
			activityInventory.setSupplierName(values[17]);	
			activityInventory.setSupplierAddress(values[18]);	
			activityInventory.setSupplierTP(values[19]);	
			activityInventory.setInventoryType(values[20]);	
			activityInventory.setActivityDescription(values[21]);
			
			
			
			
			try {
				searchInfoDeMap.get(values[0]).addInventoryInfo(activityInventory);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return searchInfoDeMap;
	}
	
	public static Map<Integer,ActivityInfo> convertIntoMap(java.util.List<ActivityInfo> inventoryList){
		
		Map<Integer,ActivityInfo> inventoryListMap=new HashMap<Integer, ActivityInfo>();
		
		for(ActivityInfo r:inventoryList){			
			inventoryListMap.put(Integer.parseInt(r.getScenarioCount()),r);
		}
		
		return inventoryListMap;
		
	}
	
	
}
