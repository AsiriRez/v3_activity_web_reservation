package reservationreport;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.model.ActivityDetails;

public class LoadReservations {
	
	private WebDriver driver;
	private ActivityDetails activitydetails;
	private String currentDateforImages;
	private Reservation reservationDetails;
	
	
	public LoadReservations(ActivityDetails activityDetails, WebDriver Driver){
		
		this.activitydetails = activityDetails;
		this.driver = Driver;
		
	}
	
	public Reservation getReservationReportDetails(WebDriver Driver) throws InterruptedException{
		
		WebDriverWait wait = new WebDriverWait(driver, 90);	
		ReservationInfoDetails rInfo = new ReservationInfoDetails(activitydetails, driver);
		reservationDetails = rInfo.viewReservationReport(activitydetails.getReservationNo(), driver);		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());
		
		
		try {
			
			if (driver.findElements(By.id("reportHeaderRow1")).size() != 0) {
				
				File CCE = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(CCE, new File(""+currentDateforImages+"/"+activitydetails.getScenarioCount()+"_ReservationReport.png"));
				
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame("reportIframe");
				
				String BookingDate = driver.findElement(By.xpath(".//*[@id='bookingdate_1']")).getText().substring(1);
				reservationDetails.setBookingDate(BookingDate);
				
				String CanPolicyDueDate = driver.findElement(By.xpath(".//*[@id='confduedate_1']")).getText().substring(1);
				reservationDetails.setCanPolicyDueDate(CanPolicyDueDate);
				
				String CustomerName_1 = driver.findElement(By.xpath(".//*[@id='customername_1']")).getText().substring(1);
				reservationDetails.setCustomerName_1(CustomerName_1);
				
				String SupplierName = driver.findElement(By.xpath(".//*[@id='suppliername_1']")).getText();
				reservationDetails.setSupplierName(SupplierName);
				
				String ProductType = driver.findElement(By.xpath(".//*[@id='producttype_1']")).getText();
				reservationDetails.setProductType(ProductType);
				
				String BookingStatus = driver.findElement(By.xpath(".//*[@id='bookingstatus_1']")).getText();
				reservationDetails.setBookingStatus(BookingStatus);
				
				String BookingChannel = driver.findElement(By.xpath(".//*[@id='bookingchanel_1']")).getText();
				reservationDetails.setBookingChannel(BookingChannel);
				
				String CustomerName_2 = driver.findElement(By.xpath(".//*[@id='guestname_1']")).getText();
				reservationDetails.setCustomerName_2(CustomerName_2);
				
				String CustomerTP = driver.findElement(By.xpath(".//*[@id='contatcno_1']")).getText();
				reservationDetails.setCustomerTP(CustomerTP);
				
				String PaymentType = driver.findElement(By.xpath(".//*[@id='approvalcode_1']")).getText().split("/")[0];
				reservationDetails.setPaymentType(PaymentType);
				
				String PayRefernce;
								
				try {
					PayRefernce = driver.findElement(By.xpath(".//*[@id='approvalcode_1']")).getText().split("/")[1];
				} catch (Exception e) {
					PayRefernce = "Not Available";
				}
				reservationDetails.setPayRefernce(PayRefernce);
				
				String AuthCode = driver.findElement(By.xpath(".//*[@id='transactionid_1']")).getText().split("/")[1];
				reservationDetails.setAuthCode(AuthCode);
				
				String VoucherIssued = driver.findElement(By.xpath(".//*[@id='voucherissued_1']")).getText();
				reservationDetails.setVoucherIssued(VoucherIssued);
				
				String InvoiceIssued = driver.findElement(By.xpath(".//*[@id='invoiceissued_1']")).getText();
				reservationDetails.setInvoiceIssued(InvoiceIssued);
				
				String InvoiceIssuedDate = driver.findElement(By.xpath(".//*[@id='invoicedate_1']")).getText();
				reservationDetails.setInvoiceIssuedDate(InvoiceIssuedDate);
				
				String LpoRequired = driver.findElement(By.xpath(".//*[@id='lporequired_1']")).getText();
				reservationDetails.setLpoRequired(LpoRequired);
				
				String LpoProvided = driver.findElement(By.xpath(".//*[@id='lpono_1']")).getText();
				reservationDetails.setLpoProvided(LpoProvided);
				
				String LpoNo = driver.findElement(By.xpath(".//*[@id='lonumber_1']")).getText();
				reservationDetails.setLpoNo(LpoNo);
				
				String SellingCurrency_1 = driver.findElement(By.xpath(".//*[@id='sellingcurrency1_1']")).getText().substring(1);
				reservationDetails.setSellingCurrency_1(SellingCurrency_1);
				
				String TotalRate = driver.findElement(By.xpath(".//*[@id='totalrate_1']")).getText();
				
				if (TotalRate.contains(",")) {
					
					String newTotalRate = TotalRate.replace(",", "").split("\\.")[0];
					reservationDetails.setTotalRate(newTotalRate);
					
				} else {
					
					String newTotalRate = TotalRate.split("\\.")[0];
					reservationDetails.setTotalRate(newTotalRate);
				}
				
				
				
				String CreditCardFee = driver.findElement(By.xpath(".//*[@id='creditcardfee_1']")).getText().split("\\.")[0];
				reservationDetails.setCreditCardFee(CreditCardFee);
				
				String BookingFeeCharges = driver.findElement(By.xpath(".//*[@id='bookingfeencharge_1']")).getText().split("\\.")[0];
				reservationDetails.setBookingFeeCharges(BookingFeeCharges);
				
				String AmountPaid = driver.findElement(By.xpath(".//*[@id='amountpaid_1']")).getText().split("\\.")[0];
				reservationDetails.setAmountPaid(AmountPaid);
				
				String SellingCurrency_2 = driver.findElement(By.xpath(".//*[@id='sellingcurrency2_1']")).getText().substring(1);
				reservationDetails.setSellingCurrency_2(SellingCurrency_2);
				
				String GrossOrderValue = driver.findElement(By.xpath(".//*[@id='grossordervalue_1']")).getText();	
				
				if (GrossOrderValue.contains(",")) {
					
					String newGrossOrderValue = GrossOrderValue.replace(",", "").split("\\.")[0];
					reservationDetails.setGrossOrderValue(newGrossOrderValue);
					
				} else {
					
					String newGrossOrderValue = GrossOrderValue.split("\\.")[0];
					reservationDetails.setGrossOrderValue(newGrossOrderValue);
				}
			
				
				if (driver.findElement(By.xpath(".//*[@id='agentcomission_1']")).getText().equalsIgnoreCase("N/A")) {
					reservationDetails.setAgentCommission("0");
				} else {
					String AgentCommission = driver.findElement(By.xpath(".//*[@id='agentcomission_1']")).getText().split("\\.")[0];
					reservationDetails.setAgentCommission(AgentCommission);
				}
				
				String NetOrderValue = driver.findElement(By.xpath(".//*[@id='netordervalue1_1']")).getText().split("\\.")[0];
				
				if (NetOrderValue.contains(",")) {
					
					String newNetOrderValue = NetOrderValue.replace(",", "").split("\\.")[0];
					reservationDetails.setNetOrderValue(newNetOrderValue);
					
				} else {
					
					String newNetOrderValue = NetOrderValue.split("\\.")[0];
					reservationDetails.setNetOrderValue(newNetOrderValue);
				}
			
				
				String TotalCost = driver.findElement(By.xpath(".//*[@id='totalcost1_1']")).getText();
				
				if (TotalCost.contains(",")) {
					
					String newTotalCost = TotalCost.replace(",", "").split("\\.")[0];
					reservationDetails.setTotalCost(newTotalCost);
					
				} else {
					
					String newTotalCost = TotalCost.split("\\.")[0];
					reservationDetails.setTotalCost(newTotalCost);
				}
				
			
				
				String BaseCurrency = driver.findElement(By.xpath(".//*[@id='basecurrency_1']")).getText().substring(1);
				reservationDetails.setBaseCurrency(BaseCurrency);
				
				String BaseCurrency_NetOrgerValue = driver.findElement(By.xpath(".//*[@id='netordervalue2_1']")).getText().split("\\.")[0];
				reservationDetails.setBaseCurrency_NetOrgerValue(BaseCurrency_NetOrgerValue);
				
				String BaseCurrency_TotalCost = driver.findElement(By.xpath(".//*[@id='totalcost2_1']")).getText().substring(1).split("\\.")[0];
				reservationDetails.setBaseCurrency_TotalCost(BaseCurrency_TotalCost);
				
				String PageTotal_1 = driver.findElement(By.xpath(".//*[@id='pagetotal']")).getText();
				reservationDetails.setPageTotal_1(PageTotal_1);
				
				String PageTotal_2 = driver.findElement(By.xpath(".//*[@id='pagetotal2']")).getText();
				reservationDetails.setPageTotal_2(PageTotal_2);
				
				String GrandTotal_1 = driver.findElement(By.xpath(".//*[@id='grandvalue']")).getText();
				reservationDetails.setGrandTotal_1(GrandTotal_1);
				
				String GrandTotal_2 = driver.findElement(By.xpath(".//*[@id='grandvalue2']")).getText();
				reservationDetails.setGrandTotal_2(GrandTotal_2);
				
				
			} else {
				reservationDetails.setReservationReportLoaded(false);
			}
			
	
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		return reservationDetails;
	}
	
	
	
}
